<?php

include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

include 'pn_android.php';
include 'pn_iphone.php';

function sortByOrder($a, $b) 
{
    return $a['distance'] - $b['distance'];
}

$ride_id=$_REQUEST['ride_id'];
$driver_id=$_REQUEST['driver_id'];
$ride_status=$_REQUEST['ride_status'];
$driver_token=$_REQUEST['driver_token'];
$language_id=$_REQUEST['language_id'];

if($ride_id != "" && $driver_id != "" && $ride_status != "" && $driver_token!= ""  )
{
	$query="select * from driver where driver_token='$driver_token'";
	$result = $db->query($query);
	$ex_rows=$result->num_rows;
	if($ex_rows==1)
	{
		$dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
		$date=$dt->format('M j, Y');
		$day=date("l");
		$date=$day.", ".$date;
		$new_time=date("H:i");
		$query8="UPDATE driver SET last_update='$new_time',last_update_date='$date' WHERE driver_id='$driver_id'" ;
		$db->query($query8);
					
		$query2="INSERT INTO ride_reject (reject_ride_id, reject_driver_id) VALUES('$ride_id','$driver_id')" ;
		$db->query($query2);

		$query3="select * from driver WHERE driver_id='$driver_id'" ;
		$result3 = $db->query($query3);	
		$list3=$result3->row;
		$reject_rides=$list3['reject_rides']+"1";
		$query4="UPDATE driver SET reject_rides='$reject_rides' WHERE driver_id='$driver_id'" ;
		$db->query($query4);
		
		$query35="select * from ride_reject WHERE reject_ride_id='$ride_id'";
		$result35 = $db->query($query35);	
		$ex_rows=$result35->num_rows;
		
		
		if($ex_rows==1)
		{
		
		//echo "ex_rows  ".$ex_rows;
			$query5="select * from ride_table where ride_id='$ride_id'";
			$result5 = $db->query($query5);
			$list5=$result5->row;
			
			$pickup_lat=$list5['pickup_lat'];
			$pickup_long=$list5['pickup_long'];
		    	$car_type_id=$list5['car_type_id'];
			
			$query3="select * from driver where car_type_id='$car_type_id' and online_offline = 1 and status=1 and busy=0 and login_logout=1";
			$result3 = $db->query($query3);
			$list3=$result3->rows;
			
	
	
			$info 	= $_REQUEST;
			unset($info['PHPSESSID']);
	
			$c = array();
			foreach($list3 as $login3)
			{
                $driver_lat = $login3['current_lat'];
                $driver_long =$login3['current_long'];
		 
				$theta = $pickup_long - $driver_long;
				$dist = sin(deg2rad($pickup_lat)) * sin(deg2rad($driver_lat)) +  cos(deg2rad($pickup_lat)) * cos(deg2rad($driver_lat)) * cos(deg2rad($theta));
				$dist = acos($dist);
				$dist = rad2deg($dist);
				$miles = $dist * 60 * 1.1515;
				$unit = strtoupper("K");
				if ($unit == "K") 
				{
					$km=$miles* 1.609344;
				} 
				else if ($unit == "N") 
				{
      		        $miles * 0.8684;
				} 
				else 
				{
					$miles;
				}
				if($km<= 10)
				{
					$c[] = array("driver_id"	=> $login3['driver_id'],"distance" => $km,);
				}
			}
			usort($c, 'sortByOrder');
			$size=sizeof($c);
			
			//print_r ($c);
			
			if($size>0)
			{
				$driver_id = $c[0]['driver_id'];
				
				//echo "id ".$driver_id;
			
				$query4="select * from ride_allocated where allocated_ride_id='$ride_id' and allocated_driver_id='$driver_id'";
				$result4 = $db->query($query4);
				$ex_rows=$result4->num_rows;
			
				if($ex_rows==1)
				{
					if($size>1)
					{
						$driver_id = $c[1]['driver_id'];
						
							//echo "id 2".$driver_id;
						
						$query5="INSERT INTO ride_allocated (allocated_ride_id,allocated_driver_id) VALUES ('$ride_id','$driver_id')";
						$db->query($query5); 
		
						$query4="select * from driver where driver_id='$driver_id'";
						$result4 = $db->query($query4);
						$list4=$result4->row;
						
						//print_r ($list4);
		
						$device_id=$list4['device_id'];
						
					
						//$message="New Ride Allocated"."_".$ride_id."_1";
						
						 $message="New Ride Allocated";
                				$ride_id= (String) $ride_id;
                				$ride_status= "1";

						if($device_id!="")
						{	
							if($list4['flag'] == 1)
							{
								IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status);
							} 
							else 
							{ 
								AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
							} 
						}
					}
					else 
					{
						$query11="UPDATE ride_table SET driver_id='$driver_id' , ride_status='$ride_status' WHERE ride_id='$ride_id'" ;
						$db->query($query11);
			
						$query5="select * from ride_table where ride_id='$ride_id'";
						$result5 = $db->query($query5);
						$list5=$result5->row;
						$user_id=$list5['user_id'];
	
						$query6="select * from user where user_id='$user_id'";
						$result6= $db->query($query6);
						$list6=$result6->row;
						$device_id=$list6['device_id'];

						//$message="Booking Rejected"."_".$ride_id."_".$ride_status;
						
						$message="Booking Rejected";
                				$ride_id= (String) $ride_id;
                				$ride_status= (String) $ride_status;
						
						
						if($device_id!="")
						{
							if($list6['flag'] == 1)
							{
								IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
							}	 
							else 
							{ 	
								AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
							} 	
						}
					}
				}
				else 
				{
					$query5="INSERT INTO ride_allocated (allocated_ride_id,allocated_driver_id) VALUES ('$ride_id','$driver_id')";
					$db->query($query5); 
		
					$query4="select * from driver where driver_id='$driver_id'";
					$result4 = $db->query($query4);
					$list4=$result4->row;
		
					$device_id=$list4['device_id'];
					
					$message="New Ride Allocated";
                				$ride_id= (String) $ride_id;
                				$ride_status= "1";


					if($device_id!="")
					{	
						if($list4['flag'] == 1)
						{
                    		IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status);
						} 
						else 
						{  
							AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
						} 
					}
				}
			}
			else 
			{
				$query11="UPDATE ride_table SET driver_id='$driver_id' , ride_status='$ride_status' WHERE ride_id='$ride_id'" ;
				$db->query($query11);
			
				$query5="select * from ride_table where ride_id='$ride_id'";
				$result5 = $db->query($query5);
				$list5=$result5->row;
				$user_id=$list5['user_id'];

				$query6="select * from user where user_id='$user_id'";
				$result6= $db->query($query6);
				$list6=$result6->row;
				$device_id=$list6['device_id'];

				//$message="Booking Rejected"."_".$ride_id."_".$ride_status;
				
				
						$message="Booking Rejected";
                				$ride_id= (String) $ride_id;
                				$ride_status= (String) $ride_status;
				
				
				if($device_id!="")
				{
					if($list6['flag'] == 1)
					{
						IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
					}	 
					else 
					{ 	 
						AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
					} 	
				}
			}
		}
		else if($ex_rows==2)
		{
		
			//echo "ex_rows  2";
			$query11="UPDATE ride_table SET driver_id='$driver_id' , ride_status='$ride_status' WHERE ride_id='$ride_id'" ;
			$db->query($query11);
			
			$query5="select * from ride_table where ride_id='$ride_id'";
			$result5 = $db->query($query5);
			$list5=$result5->row;
			$user_id=$list5['user_id'];

			$query6="select * from user where user_id='$user_id'";
			$result6= $db->query($query6);
			$list6=$result6->row;
			$device_id=$list6['device_id'];

			$message="Booking Rejected";
                				$ride_id= (String) $ride_id;
                				$ride_status= (String) $ride_status;
			if($device_id!="")
			{
				if($list6['flag'] == 1)
				{
					IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
				}	 
				else 
				{  
					AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
				} 	
			}
		}
		
		$re = array('result'=> 1,'msg'=> "Ride Rejected Successfully!!",);
	}
	else 
	{
		$re = array('result'=> 419,'msg'=> "No Record Found",);
	}
}
else 
{
	$re = array('result' => 0,'msg'	=> "Required fields missing!!",);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>
