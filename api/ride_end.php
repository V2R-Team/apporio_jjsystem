<?php

include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

include 'pn_android.php';
include 'pn_iphone.php';

$ride_id=$_REQUEST['ride_id'];
$driver_id=$_REQUEST['driver_id'];
$begin_lat=$_REQUEST['begin_lat'];
$begin_long=$_REQUEST['begin_long'];
$begin_location=$_REQUEST['begin_location'];
$end_lat=$_REQUEST['end_lat'];
$end_long=$_REQUEST['end_long'];
$end_location=$_REQUEST['end_location'];
$end_time=$_REQUEST['end_time'];
$waiting_time=$_REQUEST['waiting_time'];
$ride_time=$_REQUEST['ride_time'];
$ride_status=$_REQUEST['ride_status'];
$distance=$_REQUEST['distance'];
$driver_token=$_REQUEST['driver_token'];
$language_id=$_REQUEST['language_id'];

if($ride_id!="" && $driver_id!="" && $end_lat!="" && $end_long!="" && $end_location!="" && $end_time!="" && $waiting_time!="" && $ride_time!="" && $ride_status!="" && $driver_token!= "" ) 
{
	 $query="select * from driver where driver_token='$driver_token'";
	$result = $db->query($query);
	$ex_rows=$result->num_rows;
	if($ex_rows==1)
	{
		
		
		$dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
	$data=$dt->format('M j'); 
	$day=date("l");
	$date=$day.", ".$data ;
	$new_time=date("h:i");
	
	$query5="UPDATE driver SET last_update='$new_time' WHERE driver_id='$driver_id'" ;
					$db->query($query5);
	
	$query1="UPDATE ride_table SET driver_id='$driver_id' , ride_status='$ride_status' WHERE ride_id='$ride_id'" ;
	$db->query($query1);

    $query3="select * from driver WHERE driver_id='$driver_id'" ;
    $result3 = $db->query($query3);	
    $list3=$result3->row;
    $completed_rides=$list3['completed_rides']+"1";
	$city_id=$list3['city_id'];
	$car_type_id=$list3['car_type_id'];
    $query4="UPDATE driver SET completed_rides='$completed_rides' WHERE driver_id='$driver_id'" ;
    $db->query($query4);
	
	$query2="select * from rate_card where city_id='$city_id' and car_type_id='$car_type_id'";
	$result2 = $db->query($query2);
	$list3=$result2->row;	
	$base_miles=$list3['base_miles'];
	$base_minutes=$list3['base_minutes'];
	$base_price_miles=$list3['base_price_miles'];
	$base_price_minute=$list3['base_price_minute'];
	$price_per_mile=$list3['price_per_mile'];
	$price_per_minute=$list3['price_per_minute'];
	
	$query3="select * from bill_calculate where ride_id='$ride_id'";
	$result3 = $db->query($query3);
	$list3=$result3->rows;

	foreach($list3 as $login3)
        {	
		$driver_lat=$login3['driver_lat'];
		$driver_long=$login3['driver_long'];
		$c[] = array("driver_lat"	=> $driver_lat,"driver_long" => $driver_long,);
	}
	
	$dis= array();
	for ($i = 0; $i <(count($c)-1) ; $i++) 
	{	
		$driver_lat_first = $c[$i]['driver_lat'];
		$driver_long_first = $c[$i]['driver_long'];
		
		$driver_lat_second = $c[$i+1]['driver_lat'];
		$driver_long_second = $c[$i+1]['driver_long'];
		
		$theta = $driver_long_first - $driver_long_second;
  		$dist = sin(deg2rad($driver_lat_first)) * sin(deg2rad($driver_lat_second)) +  cos(deg2rad($driver_lat_first)) * cos(deg2rad($driver_lat_second)) * cos(deg2rad($theta));
  		$dist = acos($dist);
  		$dist = rad2deg($dist);
  		$miles = $dist * 60 * 1.1515;
  		$unit = strtoupper("M");
  		if ($unit == "K") 
  		{
			//$km=$miles* 1.609344;
  		} 
  		else if ($unit == "N") 
  		{
      		$miles * 0.8684;
  		} 
  		else 
  		{
	  		$miles;
            $dis[]=$miles;
  		}
	}
	$dist1=array_sum($dis);
        $dist1= number_format($dist1, 2, '.', '');
	
	if($dist1<=$base_miles)
	{
		$final_amount=$base_price_miles;
	}
	else 
	{
		$diff_distance=$dist1-$base_miles;
			$amount1=($diff_distance * $price_per_mile);
			$final_amount=$base_price_miles+$amount1;
	}
	
	$query2="UPDATE done_ride SET end_lat='$end_lat',end_long='$end_long', end_location='$end_location', end_time='$end_time', waiting_time='$waiting_time', ride_time='$ride_time', amount='$final_amount', distance='$dist1',tot_time='$ride_time' WHERE ride_id='$ride_id'";
    $db->query($query2);

	$query3="select * from done_ride where ride_id='$ride_id'";
	$result3 = $db->query($query3);
	$list=$result3->row;
	$done_ride=$list['done_ride_id'];

    $query4="select * from ride_table where ride_id='$ride_id'";
	$result4 = $db->query($query4);
	$list4=$result4->row;
    $user_id=$list4['user_id'];

$payment_option_id=$list4['payment_option_id'];

$list['payment_option_id']=$payment_option_id;



    $query5="select * from user where user_id='$user_id'";
    $result5 = $db->query($query5);
	$list5=$result5->row;
    $device_id=$list5['device_id'];

    //$message="Trip has ended"."_".$done_ride."_".$ride_status;

$message="Ride has Ended";
$ride_id= (String) $done_ride;
                $ride_status= (String) $ride_status;

    if($device_id!="")
    {
	       if($list5['flag'] == 1)
               {
                     IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
	       		} 
               else 
               {  
		    AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
	       } 
        }	
		
	$re = array('result'=> 1,'msg'=> "Ride Has been Ended",'details'	=> $list);	
	
		}
	else {
			$re = array('result'=> 419,'msg'=> "No Record Found",);
	}
}
else
{
   $re = array('result'=> 0,'msg'=> "Required fields missing!!",);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>