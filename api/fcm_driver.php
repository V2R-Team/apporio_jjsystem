<?php
function AndroidPushNotification($did, $msg) {
	// Set POST variables
	$url = 'https://fcm.googleapis.com/fcm/send';
	
	$fields = array (
			'to' 		        => $did,
			'data' => array ("message" => $msg,'msg'=>"Hello") 
	);
	$headers = array (
			'Authorization: key=AAAAaY_4ROU:APA91bHuaz0kuArcLw7k025gBa7tCCtHZoho3xpJGOtmAqUHILyq0MTAYa2y2gDoBjFyAhGMcDfppf0f8UqW4sXBeLU6e74zrEf_ri-fPzNQPrrkKsRN7Eyj8pIJodAsNgziKAG5t4UJXpQMPkCfzVCnu9SmMf5ECQ',
			'Content-Type: application/json' );
	// Open connection
	$ch = curl_init ();
	// Set the url, number of POST vars, POST data
	curl_setopt ( $ch, CURLOPT_URL, $url );
	curl_setopt ( $ch, CURLOPT_POST, true );
	curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
	curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
	// Execute post
	$result = curl_exec ( $ch );
	// Close connection
	curl_close ( $ch );
        return $result;
}
?>