<?php

include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

include 'pn_android.php';
include 'pn_iphone.php';

function sortByOrder($a, $b) 
{
      return $a['distance'] - $b['distance'];
}

$user_id=$_REQUEST['user_id'];
$coupon_code=$_REQUEST['coupon_code'];
$pickup_lat=$_REQUEST['pickup_lat'];
$pickup_long=$_REQUEST['pickup_long'];
$pickup_location=$_REQUEST['pickup_location'];
$drop_lat=$_REQUEST['drop_lat'];
$drop_long=$_REQUEST['drop_long'];
$drop_location=$_REQUEST['drop_location'];
$ride_type=$_REQUEST['ride_type'];
$ride_status=$_REQUEST['ride_status'];
$car_type_id=$_REQUEST['car_type_id'];
$language_id=$_REQUEST['language_id'];
$payment_option_id=$_REQUEST['payment_option_id'];
$card_id=$_REQUEST['card_id'];

if($user_id!="" && $pickup_lat!="" && $pickup_long!="" && $pickup_location!="" && $drop_lat!="" &&
$drop_long!="" && $drop_location!="" && $ride_type!="" && $ride_status!="" && $car_type_id!="" ) 
{
	$dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
	$data=$dt->format('M j'); 
	$day=date("l");
	$date=$day.", ".$data ;
	$time=date("h:i A");

$image ="https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|".$pickup_lat.",".$pickup_long."&markers=color:red|label:D|".$drop_lat.",".$drop_long."&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4";
	
	$query1="INSERT INTO ride_table (user_id,coupon_code, pickup_lat, pickup_long,pickup_location,drop_lat,drop_long,drop_location,
	ride_date,ride_time,ride_type,ride_status,ride_image,car_type_id,status,payment_option_id,card_id) 
	VALUES ('$user_id','$coupon_code','$pickup_lat','$pickup_long','$pickup_location','$drop_lat','$drop_long','$drop_location',
	'$date','$time','1','$ride_status','$image','$car_type_id','1','$payment_option_id','$card_id')";
	
	
	$db->query($query1); 
	
	$last_id = $db->getLastId();
	
	$query3="select * from ride_table where ride_id='$last_id'";
	$result3 = $db->query($query3);
	$list=$result3->row;
	
	$query3="select * from driver where car_type_id='$car_type_id' and online_offline = 1 and status=1 and busy=0 and login_logout=1";
	$result3 = $db->query($query3);
	$ex_rows=$result3->num_rows;
	
	if($ex_rows==0)
	{
		$re = array('result'=> 0,'msg'=> "No Driver Available",);	
	}
	else {
	
	$list3=$result3->rows;
	
	$info 	= $_REQUEST;
    unset($info['PHPSESSID']);
	
	$c = array();
	
	foreach($list3 as $login3)
        {
                $driver_lat = $login3['current_lat'];
                $driver_long =$login3['current_long'];
		 
		$theta = $pickup_long - $driver_long;
  		$dist = sin(deg2rad($pickup_lat)) * sin(deg2rad($driver_lat)) +  cos(deg2rad($pickup_lat)) * cos(deg2rad($driver_lat)) * cos(deg2rad($theta));
  		$dist = acos($dist);
  		$dist = rad2deg($dist);
  		$miles = $dist * 60 * 1.1515;
  		$unit = strtoupper("K");
  		if ($unit == "K") 
  		{
			$km=$miles* 1.609344;
  		} 
  		else if ($unit == "N") 
  		{
      		        $miles * 0.8684;
  		} 
  		else 
  		{
	  		$miles;
  		}
		if($km<= 10)
		{
			$c[] = array("driver_id"	=> $login3['driver_id'],"distance" => $km,);
		}
	}
	
	        usort($c, 'sortByOrder');
		$c = $c[0]['driver_id'];
		$driver_id=$c;
		
		$query5="INSERT INTO ride_allocated (allocated_ride_id, allocated_driver_id, allocated_ride_status) VALUES ('$last_id','$driver_id','$ride_status')";
		$db->query($query5); 
		
		$query4="select * from driver where driver_id='$driver_id'";
		$result4 = $db->query($query4);
		$list4=$result4->row;
		
		$device_id=$list4['device_id'];

		$query45="select * from user WHERE user_id='$user_id'" ;
                $result45 = $db->query($query45);	
                $list45=$result45->row;
		$user_phone=$list45['user_phone'];

             /*  	$sms_message=urlencode("Your booking has been confirmed. Driver will arrive in no of Minutes. Happy Journey!");
		$sms_url = "http://bhashsms.com/api/sendmsg.php?user=changemytyre&pass=P@ssw0rD&sender=CMTYRE&phone={$user_phone}&text={$sms_message}&priority=ndnd&stype=normal";
		curl_get_contents($sms_url);*/
	
		//$message="New Ride Allocated"."_".$last_id."_".$ride_status;

                $message="New Ride Allocated";
                $ride_id= (String) $last_id;
                $ride_status= (String) $ride_status;

        	if($device_id!="")
        	{
        		if($list4['flag'] == 1)
            		{
                    		IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status);
	        	} 
            		else 
            		{  
		    		AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
	        	} 
        	}
        	else {
        		// Ignore this part
        	}
        	$re = array('result'=> 1,'msg'=> "".$km,'details'	=> $list);	
			
	}
	
}
else
{
   $re = array('result'=> 0,'msg'=> "Required fields missing!!",);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>