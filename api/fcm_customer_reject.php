<?php
function AndroidPushNotificationReject($did, $msg) {
	// Set POST variables
	$url = 'https://fcm.googleapis.com/fcm/send';
	
	$fields = array (
			'to' => $did,
                        'data' => array('message' => $msg)  
	);
	$headers = array (
			'Authorization: key=AAAADH-yOkM:APA91bGDVnlqH2QbC9GeWe9yDasSbOvtX28X7ewDiZZsgkj6Vh1zHAPAly9idCI6r5YXzMlZHE31I33aMusC-4ftKF4NzCgudWI05RxRwRFOWiLKpOWFq4J7Bj3LZm3DZ8jSP8kkyAL4A2QhjLNEqOqOK57TsesN4g',
			'Content-Type: application/json' 
	);
	// Open connection
	$ch = curl_init ();
	// Set the url, number of POST vars, POST data
	curl_setopt ( $ch, CURLOPT_URL, $url );
	curl_setopt ( $ch, CURLOPT_POST, true );
	curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
	curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
	// Execute post
 	$result = curl_exec ( $ch );
	// Close connection
	curl_close ( $ch );
        return $result;
}
?>