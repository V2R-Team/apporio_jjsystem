<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['DID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');

if(isset($_POST['save']))
{
    $department_role = "";
    $user_name = $_POST['firstname']." ".$_POST['lastname'];
    $query="select * from user WHERE user_phone='".$_POST['phone']."'";
    $result = $db->query($query);
    $list=$result->row;
    $dt = DateTime::createFromFormat('!d/m/Y',date("d/m/Y"));
       	$data=$dt->format('M j'); 
 	$day=date("l");
 	$date=$day.", ".$data ;
 	$time=date("h:i A");
       	$date_add=$date.", ".$time;

    if(count($list) == 0)
    {
        $query2="INSERT INTO user (company_id,branch_id,department_id,user_name,user_phone,user_email,employee_code,user_password,register_date)
 VALUES ('".$_SESSION['ADMIN']['DCID']."','".$_SESSION['ADMIN']['DBID']."','".$_SESSION['ADMIN']['DID']."','$user_name','".$_POST['phone']."','".$_POST['email']."','".$_POST['employee_code']."','".$_POST['password']."','$date_add')";
        $db->query($query2);
    }else{
        $errorMsg = "Phone Number Already Registerd!";
    }
}
?>
<!-- Page Content Start -->
<!-- ================== -->

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Add Employee Details</h3>
        <?php if(isset($errorMsg)){ ?>
            <h3 style="float:right; color:red;">Phone Number already Registerd!</h3>
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form">
                        <form class="cmxform form-horizontal tasi-form"  method="post">

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Employee ID</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control"  placeholder="Employee ID" name="employee_code" id="employee_code" required>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Firstname</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control"  placeholder="Firstname" name="firstname" id="firstname" required>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Lastname</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control"  placeholder="Lastname" name="lastname" id="lastname" required>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Email</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control"  placeholder="Email" name="email" id="email" required>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Phone</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control"  placeholder="Phone" name="phone" id="phone" required>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Default Password</label>
                                <div class="col-lg-10">
                                    <input type="password" class="form-control"  placeholder="Default Password" name="password" id="password" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- .form -->

                </div>
                <!-- panel-body -->
            </div>
            <!-- panel -->
        </div>
        <!-- col -->

    </div>
    <!-- End row -->

</div>

<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>
