<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['DID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');

$query="select * from user WHERE department_id='".$_SESSION['ADMIN']['DID']."' ORDER BY user_id DESC";
$result = $db->query($query);
$list=$result->rows;

if(isset($_GET['status']) && isset($_GET['id']))
{
    $query1="UPDATE user SET status ='".$_GET['status']."' WHERE user_id ='".$_GET['id']."'";
    $db->query($query1);
    $db->redirect("home.php?pages=view_employee");
}

if(isset($_POST['savechanges']))
{
  
    $query2="UPDATE user SET employee_code ='".$_POST['employee_code']."',user_name='".$_POST['user_name']."',user_email='".$_POST['email']."',user_phone='".$_POST['phone']."' where user_id='".$_POST['savechanges']."'";
    $db->query($query2);
    $db->redirect("home.php?pages=view_employee");

}
?>
<form method="post" name="frm">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">View Employee Details</h3>

        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Employee Code</th>
                                        <th>Status</th>
                                        <th>Edit</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                    $i = 1;
                                    foreach($list as $employee){?>
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td><?= $employee['user_name'];?></td>
                                            <td><?= $employee['user_email'];?></td>
                                            <td><?= $employee['user_phone'];?></td>
                                            <td><?= $employee['user_password'];?></td>
                                                               <?php
                                            if($employee['status']==1) {
                                                ?>
                                                <td class="text-center">
                                                    <a href="home.php?pages=view_employee&status=2&id=<?php echo $employee['user_id']?>" class="" title="Active">
                                                        <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active
                                                        </button></a>
                                                </td>
                                                <?php
                                            } else {
                                                ?>
                                                <td class="text-center">
                                                    <a href="home.php?pages=view_employee&status=1&id=<?php echo $employee['user_id']?>" class="" title="Deactive">
                                                        <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive
                                                        </button></a>
                                                </td>
                                            <?php } ?>
                                            <td><button type="button" class="btn btn-info glyphicon glyphicon-pencil" data-toggle="modal" data-target="#<?php echo $employee['user_id'];?>"  ></button></td>
                                        </tr>
                                        <?php $i++;
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End row -->

    </div>
</form>

<?php foreach($list as $employee){?>
    <div class="modal fade" id="<?php echo $employee['user_id'];?>" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Edit Employee Details</h4>
                </div>
                <form  method="post" >
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Username</label>
                                    <input type="text" class="form-control"  placeholder="Username" name="user_name" value="<?php echo $employee['user_name'];?>" id="user_name" required>
                                </div>
                            </div>
                        </div>
 

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Phone</label>
                                    <input type="text" class="form-control"  placeholder="Phone" name="user_phone" value="<?php echo $employee['user_phone'];?>" id="user_phone" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Email</label>
                                    <input type="text" class="form-control"  placeholder="Email" name="user_email" value="<?php echo $employee['user_email'];?>" id="user_email" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Employee ID</label>
                                    <input type="text" class="form-control"  placeholder="Employee ID" name="employee_code" value="<?php echo $employee['employee_code'];?>" id="employee_code" required>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" name="savechanges" value="<?php echo $employee['user_id'];?>" class="btn btn-info">Save Changes</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
<?php }?>
<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>