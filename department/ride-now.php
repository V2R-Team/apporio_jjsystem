<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['DID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');
$query="select * from ride_table INNER JOIN user ON ride_table.user_id = user.user_id where ride_table.ride_type=1 AND user.department_id='".$_SESSION['ADMIN']['DID']."' ORDER BY ride_table.ride_id DESC";
$result = $db->query($query);
$list=$result->rows;



if(isset($_GET['status']) && isset($_GET['id']))
{
    $query1="UPDATE ride_table SET status='".$_GET['status']."' WHERE ride_id='".$_GET['id']."'";
    $db->query($query1);
    $db->redirect("home.php?pages=ride-now");
}


if(isset($_POST['delete']))
{
    $query1="DELETE FROM ride_table WHERE ride_id='".$_POST['chk']."'";
    $db->query($query1);
    $db->redirect("home.php?pages=ride-now");
}


?>

<!-- Page Content Start -->
<!-- ================== -->
<form method="post" name="frm">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">Ride Now</h3>

            <?php if($_SESSION['ADMIN']['BROLE'] == ""){ ?>

                <button type="submit" name="delete" value="delete" class="btn btn-danger glyphicon glyphicon-trash"></button>
                           <?php }else{

                $query1="select * from role WHERE role_id = '".$_SESSION['ADMIN']['CROLE']."' ";
                $result1 = $db->query($query1);
                $list1=$result1->rows;
                foreach($list1 as $lists);
                $data =  json_decode(html_entity_decode($lists['role_permission']), true);
                if($data[delete_now] == 1){
                    ?>

                    <button type="submit" name="delete" value="delete" class="btn btn-danger glyphicon glyphicon-trash"></button>
                                 <?php } } ?>

        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <?php if($_SESSION['ADMIN']['BROLE'] == ""){ ?>

                                            <th width="6%">Select</th>

                                        <?php }else{

                                            $query1="select * from role WHERE role_id = '".$_SESSION['ADMIN']['CROLE']."' ";
                                            $result1 = $db->query($query1);
                                            $list1=$result1->rows;
                                            foreach($list1 as $lists);
                                            $data =  json_decode(html_entity_decode($lists['role_permission']), true);
                                            if($data[delete_now] == 1){
                                                ?>

                                                <th width="6%">Select</th>

                                            <?php } } ?>

                                        <th width="5%">S.No</th>
                                        <th>Pickup Address</th>
                                        <th>Drop Address</th>
                                        <th width="10%">Ride Date</th>
                                        <th width="10%">Ride Time</th>
                                        <th width="12%">Ride Status</th>
                                        <th width="8%">Status</th>
                                        <th width="12%">Full Details</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($list as $ridenow){?>
                                        <tr>

                                            <?php if($_SESSION['ADMIN']['BROLE'] == ""){ ?>

                                                <td><label class="option block mn" style="width: 55px;">
                       <input type="checkbox" name="chk" value="<?php echo $ridenow['ride_id']?>" onClick="uncheck()" >
                       <span class="checkbox mn"></span>
                    </label></td>

                                            <?php }else{

                                                $query1="select * from role WHERE role_id = '".$_SESSION['ADMIN']['ROLE']."' ";
                                                $result1 = $db->query($query1);
                                                $list1=$result1->rows;
                                                foreach($list1 as $lists);
                                                $data =  json_decode(html_entity_decode($lists['role_permission']), true);
                                                if($data[delete_now] == 1){
                                                    ?>

                                                    <td><label class="option block mn" style="width: 55px;">
                       <input type="checkbox" name="chk" value="<?php echo $ridenow['ride_id']?>" onClick="uncheck()" >
                       <span class="checkbox mn"></span>
                    </label></td>

                                                <?php } } ?>

                                            <td><?php echo $ridenow['ride_id'];?></td>

                                            <td>
                                                <?php
                                                $pickup_location=$ridenow['pickup_location'];
                                                if($pickup_location=='')
                                                {
                                                    echo "----";
                                                }
                                                else
                                                {
                                                    echo $pickup_location;
                                                }

                                                ?>
                                            </td>

                                            <td>
                                                <?php
                                                $drop_location=$ridenow['drop_location'];
                                                if($drop_location=='')
                                                {
                                                    echo "----";
                                                }
                                                else
                                                {
                                                    echo $drop_location;
                                                }

                                                ?>
                                            </td>

                                            <td>
                                                <?php
                                                $ride_date=$ridenow['ride_date'];
                                                if($ride_date=='')
                                                {
                                                    echo "----";
                                                }
                                                else
                                                {
                                                    echo $ride_date;
                                                }

                                                ?>
                                            </td>

                                            <td>
                                                <?php
                                                $ride_time=$ridenow['ride_time'];
                                                if($ride_time=='')
                                                {
                                                    echo "----";
                                                }
                                                else
                                                {
                                                    echo $ride_time;
                                                }

                                                ?>
                                            </td>

                                            <td>
                                                <?php
                                                $ride_status=$ridenow['ride_status'];
                                                if($ride_status=='')
                                                {
                                                    echo "----";
                                                }
                                                else if($ride_status==1)
                                                {
                                                    echo "<strong style=\"color:#000000;\">Scheduled</strong>";
                                                }
                                                else if($ride_status==2)
                                                {
                                                    echo "<strong style=\"color:#d03f3f;\">Cancelled</strong>";
                                                }
                                                else if($ride_status==3)
                                                {
                                                    echo "Accepted";
                                                }
                                                else if($ride_status==4)
                                                {
                                                    echo "Rejected";
                                                }
                                                else if($ride_status==5)
                                                {
                                                    echo "Arrived";
                                                }
                                                else if($ride_status==6)
                                                {
                                                    echo "Started";
                                                }
                                                else if($ride_status==7)
                                                {
                                                    echo "<strong style=\"color:#47bf7b;\">Completed</strong>";
                                                }

                                                ?>
                                            </td>



                                            <?php
                                            if($ridenow['status']==1) {
                                                ?>
                                                <td class="text-center">
                                                    <a href="home.php?pages=ride-now&status=2&id=<?php echo $ridenow['ride_id']?>" class="" title="Active">
                                                        <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active
                                                        </button></a>
                                                </td>
                                                <?php
                                            } else {
                                                ?>
                                                <td class="text-center">
                                                    <a href="home.php?pages=ride-now&status=1&id=<?php echo $ridenow['ride_id']?>" class="" title="Deactive">
                                                        <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive
                                                        </button></a>
                                                </td>
                                            <?php } ?>



                                            <td><button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#<?php echo $ridenow['ride_id'];?>"  > Full Details </button></td>
                                        </tr>
                                    <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End row -->

    </div>
</form>
<!--Ride Now Details Starts-->
<?php
$dummyImg="http://apporio.co.uk/apporiotaxi/uploads/driver/driverprofile.png";
foreach($list as $ridenow){?>
    <div class="modal fade" id="<?php echo $ridenow['ride_id'];?>" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content starts-->

            <div class="modal-content">

                <div class="modal-header">
                    <div class="col-md-12"><button type="button" class="close" data-dismiss="modal">&times;</button></div>

                    <h4 class="modal-title fdetailsheading col-md-6">Driver Details</h4>
                    <h4 class="modal-title fdetailsheading col-md-6">User Details</h4>
                </div>
                <div class="modal-body">
                    <div class="modal-body">
                        <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                            <thead>

                            <tr>
                                <?php
                                $driver_id=$ridenow['driver_id'];

                                $query="select * from driver where driver_id=$driver_id";
                                $result = $db->query($query);
                                $list=$result->row; ?>

                                <td class="" colspan="" style="text-align-last:left; width:150px;"><img src="<?php


                                    if($list['driver_image'] != '' && isset($list['driver_image']))
                                    {
                                        echo '../'.$list['driver_image'];
                                    }
                                    else
                                    {
                                        echo $dummyImg;
                                    }
                                    ?>"  width="120px" height="120px">
                                </td>
                                <td><table style="width:90%; height:150px;" aling="center" border="0">
                                        <tbody>
                                        <tr>
                                            <th class="">Name</th>
                                            <td class="">
                                                <?php

                                                $driver_name=$list['driver_name'];
                                                if($driver_name=="")
                                                {
                                                    echo " --------------- ";
                                                }
                                                else
                                                {
                                                    echo $driver_name;
                                                }
                                                ?> </td>
                                        </tr>

                                        <tr>
                                            <th class="">Email</th>
                                            <td class="">
                                                <?php

                                                $driver_email=$list['driver_email'];
                                                if($driver_email=="")
                                                {
                                                    echo " --------------- ";
                                                }
                                                else
                                                {
                                                    echo $driver_email;
                                                }
                                                ?>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th class="">Phone</th>
                                            <td class="">
                                                <?php



                                                $driver_phone=$list['driver_phone'];
                                                if($driver_phone=="")
                                                {
                                                    echo " --------------- ";
                                                }
                                                else
                                                {
                                                    echo $driver_phone;
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table></td>
                                <?php

                                $user_id=$ridenow['user_id'];

                                $query="select * from user where user_id=$user_id";
                                $result = $db->query($query);
                                $list=$result->row;
                                ?>
                                <td class="" colspan="" style="text-align-last:left; width:150px;"><img src="<?php


                                    if($list['user_image'] != '' && isset($list['user_image']))
                                    {
                                        echo '../'.$list['user_image'];
                                    }
                                    else
                                    {
                                        echo $dummyImg;
                                    }
                                    ?>"  width="120px" height="120px">
                                </td>
                                <td><table style="width:90%; height:150px;" aling="center" border="0">
                                        <tbody>
                                        <tr>

                                            <th class="">Name</th>
                                            <td class="">
                                                <?php


                                                $user_name=$list['user_name'];
                                                if($user_name=="")
                                                {
                                                    echo " -------- ";
                                                }
                                                else
                                                {
                                                    echo $user_name;
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="">Email</th>
                                            <td class="">
                                                <?php



                                                $user_email=$list['user_email'];
                                                if($user_email=="")
                                                {
                                                    echo " -------- ";
                                                }
                                                else
                                                {
                                                    echo $user_email;
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="">Phone</th>
                                            <td class="">
                                                <?php



                                                $user_phone=$list['user_phone'];
                                                if($user_phone=="")
                                                {
                                                    echo " -------- ";
                                                }
                                                else
                                                {
                                                    echo $user_phone;
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table></td>
                            </tr>

                            <tr>
                                <th class="">Pickup Location</th>
                                <td colspan="3" class="">
                                    <?php
                                    $pickup_location=$ridenow['pickup_location'];
                                    if($pickup_location=='')
                                    {
                                        echo "----";
                                    }
                                    else
                                    {
                                        echo $pickup_location;
                                    }
                                    ?>
                                </td>
                            </tr>

                            <tr>
                                <th class="">Drop Location</th>
                                <td colspan="3" class="">
                                    <?php
                                    $drop_location=$ridenow['drop_location'];
                                    if($drop_location=='')
                                    {
                                        echo "----";
                                    }
                                    else
                                    {
                                        echo $drop_location;
                                    }
                                    ?>
                                </td>
                            </tr>

                            <tr>
                                <th class="">Ride Date</th>
                                <td colspan="3" class="">
                                    <?php
                                    $ride_date=$ridenow['ride_date'];
                                    if($ride_date=='')
                                    {
                                        echo "----";
                                    }
                                    else
                                    {
                                        echo $ride_date;
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <th class="">Ride Time</th>
                                <td colspan="3" class="">
                                    <?php
                                    $ride_time=$ridenow['ride_time'];
                                    if($ride_time=='')
                                    {
                                        echo "----";
                                    }
                                    else
                                    {
                                        echo $ride_time;
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <th class="">Car Type</th>
                                <td colspan="3" class="">
                                    <?php

                                    $car_type_id=$ridenow['car_type_id'];

                                    $query="select * from car_type where car_type_id=$car_type_id";
                                    $result = $db->query($query);
                                    $list=$result->row;


                                    $car_type_name=$list['car_type_name'];
                                    if($car_type_name=="")
                                    {
                                        echo " --------------- ";
                                    }
                                    else
                                    {
                                        echo $car_type_name;
                                    }
                                    ?>
                                </td>
                            </tr>

                            <tr class="">
                                <th class="">Ride Status</th>
                                <td colspan="3" class="">
                                    <?php
                                    $ride_status=$ridenow['ride_status'];
                                    if($ride_status=='')
                                    {
                                        echo "----";
                                    }
                                    else if($ride_status==1)
                                    {
                                        echo "<strong style=\"color:#000000;\">Scheduled</strong>";
                                    }
                                    else if($ride_status==2)
                                    {
                                        echo "<strong style=\"color:#d03f3f;\">Cancelled</strong>";
                                    }
                                    else if($ride_status==3)
                                    {
                                        echo "Accepted";
                                    }
                                    else if($ride_status==4)
                                    {
                                        echo "Rejected";
                                    }
                                    else if($ride_status==5)
                                    {
                                        echo "Arrived";
                                    }
                                    else if($ride_status==6)
                                    {
                                        echo "Started";
                                    }
                                    else if($ride_status==7)
                                    {
                                        echo "<strong style=\"color:#47bf7b;\">Completed</strong>";
                                    }

                                    ?>
                                </td>
                            </tr>

                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

            <!-- Modal content closed-->

        </div>
    </div>
<?php }?>
<!--Ride Now Details Closed-->

<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>