<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['DID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');

$query= "SELECT * FROM `done_ride` d
   INNER JOIN ride_table r ON r.ride_id = d.ride_id
   INNER JOIN driver dr ON dr.driver_id = d.driver_id
   INNER JOIN user u ON u.user_id = r.user_id
   INNER JOIN car_type c ON c.car_type_id = r.car_type_id
   INNER JOIN rate_card rc ON rc.car_type_id= c.car_type_id WHERE u.department_id ='".$_SESSION['ADMIN']['DID']."'";

$result = $db->query($query);
$list=$result->rows;
$basefare = 40;
?>


<form method="post" name="frm">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">Completed Rides</h3>

            <?php if($_SESSION['ADMIN']['BROLE'] == ""){ ?>

                <button type="submit" name="delete" value="delete" class="btn btn-danger glyphicon glyphicon-trash"></button>

            <?php }else{

                $query1="select * from role WHERE role_id = '".$_SESSION['ADMIN']['ROLE']."' ";
                $result1 = $db->query($query1);
                $list1=$result1->rows;
                foreach($list1 as $lists1);
                $data =  json_decode(html_entity_decode($lists1['role_permission']), true);
                if($data[ride_delete] == 1){
                    ?>

                    <button type="submit" name="delete" value="delete" class="btn btn-danger glyphicon glyphicon-trash"></button>

                <?php } } ?>


        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>

                                        <?php if($_SESSION['ADMIN']['BROLE'] == ""){ ?>

                                            <!--<th width="6%">Select</th>-->

                                        <?php }else{

                                            $query1="select * from role WHERE role_id = '".$_SESSION['ADMIN']['ROLE']."' ";
                                            $result1 = $db->query($query1);
                                            $list1=$result1->rows;
                                            foreach($list1 as $lists1);
                                            $data =  json_decode(html_entity_decode($lists1['role_permission']), true);
                                            if($data[ride_delete] == 1){
                                                ?>

                                                <!--<th width="6%">Select</th>-->

                                            <?php } } ?>


                                        <th width="5%">S.No</th>
                                        <th>Pickup Address</th>
                                        <th>Drop Address</th>
                                        <th width="10%">Ride Date</th>
                                        <th width="10%">Ride Time</th>
                                        <th width="12%">Invoice</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($list as $ridenow){?>
                                        <tr>

                                            <?php if($_SESSION['ADMIN']['BROLE'] == ""){ ?>

                                               <td><label class="option block mn" style="width: 55px;">
                          <input type="checkbox" name="chk" value="<?php echo $ridenow['done_ride_id']?>" onClick="uncheck()" >
                          <span class="checkbox mn"></span> </label></td>

                                            <?php }else{

                                                $query1="select * from role WHERE role_id = '".$_SESSION['ADMIN']['ROLE']."' ";
                                                $result1 = $db->query($query1);
                                                $list1=$result1->rows;
                                                foreach($list1 as $lists1);
                                                $data =  json_decode(html_entity_decode($lists1['role_permission']), true);
                                                if($data[ride_delete] == 1){
                                                    ?>

                                                    <td><label class="option block mn" style="width: 55px;">
                          <input type="checkbox" name="chk" value="<?php echo $ridenow['done_ride_id']?>" onClick="uncheck()" >
                          <span class="checkbox mn"></span> </label></td>

                                                <?php } } ?>


                                            <td><?php echo $ridenow['done_ride_id'];?></td>
                                            <td><?php
                                                $begin_location=$ridenow['begin_location'];
                                                if($begin_location=='')
                                                {
                                                    echo "------";
                                                }
                                                else
                                                {
                                                    echo $begin_location;
                                                }

                                                ?></td>
                                            <td><?php
                                                $end_location=$ridenow['end_location'];
                                                if($end_location=='')
                                                {
                                                    echo "------";
                                                }
                                                else
                                                {
                                                    echo $end_location;
                                                }

                                                ?></td>
                                            <td><?php
                                                $ride_date=$ridenow['ride_date'];
                                                if($ride_date=='')
                                                {
                                                    echo "------";
                                                }
                                                else
                                                {
                                                    echo $ride_date;
                                                }

                                                ?></td>
                                            <td><?php
                                                $ride_time=$ridenow['ride_time'];
                                                if($ride_time=='')
                                                {
                                                    echo "------";
                                                }
                                                else
                                                {
                                                    echo $ride_time;
                                                }

                                                ?></td>
                                            <td><button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#test<?php echo $ridenow['done_ride_id'];?>"  > Invoice </button></td>
                                        </tr>
                                    <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End row -->

    </div>
</form>


<?php
$dummyImg="http://apporio.co.uk/apporiotaxi/uploads/driver/driverprofile.png";



foreach($list as $ridenow){?>
    <div class="modal fade" id="test<?php echo $ridenow['done_ride_id'];?>" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content starts-->

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Invoice Full Details #<?php echo $ridenow['done_ride_id'];?></h4>
                </div>
                <form  method="post"  onSubmit="return validatelogin()">
                    <div class="modal-body" /*style="max-height: 500px; overflow: auto;"*/>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                <tr>
                                    <td><table width="100%" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td><?php echo $ridenow['ride_date']; ?></td>
                                                <td><div align="right"><img src="http://apporio.co.uk/apporiotaxi/admin/images/invoice_logo.png" width="27" height="20"/></div></td>
                                            </tr>
                                        </table></td>
                                </tr>

                                <tr>
                                    <td><div align="center"><strong style="font-size:30px;">Rs. <?php echo $basefare + $ridenow['first_2km'] + $ridenow['after_2km'] + $ridenow['extra_charges'] + $ridenow['waiting_charges'] + $ridenow['time_charges']; ?></strong></div></td>
                                </tr>

                                <tr>
                                    <td><div align="center">CRN530048250  </div></td>
                                </tr>

                                <tr>
                                    <td><div align="center">Thanks for travelling with us, <strong><?php echo $ridenow['user_name']; ?></strong></div></td>
                                </tr>

                                <tr>
                                    <td height="200" style="padding:5px; width:100%; height:200px;">
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d448182.50738077075!2d77.0932634!3d28.646965499999997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1485073091200" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="padding:0px;"><table class="table-bordered" width="100%" style="border:0px !important;">
                                            <tbody>
                                            <tr>
                                                <td width="50%" align="center"><h4 class="modal-title fdetailsheading">Bill Details</h4></td>
                                                <td width="50%" align="center"><h4 class="modal-title fdetailsheading">Ride Details</h4></td>
                                            </tr>
                                            </tbody>
                                        </table></td>
                                </tr>

                                <tr>
                                    <td style="padding:0px;">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td style="padding:0px;">
                                                    <table class="table-bordered" align="left" width="50.3%" cellspacing="0" cellpadding="0" style="border-bottom:0px !important; border-left:0px !important;">
                                                        <tbody>
                                                        <tr>
                                                            <td>Base Fare</td>
                                                            <td>Rs.40</td>
                                                        </tr>
                                                        <tr>
                                                            <td>First 2km</td>
                                                            <td> Rs.
                                                                <?php

                                                                $first_2km=$ridenow['first_2km'];
                                                                if($first_2km=="")
                                                                {
                                                                    echo "------";
                                                                }
                                                                else
                                                                {
                                                                    echo $first_2km;
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>After 2km</td>
                                                            <td> Rs.
                                                                <?php
                                                                $after_2km=$ridenow['after_2km'];
                                                                if($after_2km=="")
                                                                {
                                                                    echo "------";
                                                                }
                                                                else
                                                                {
                                                                    echo $after_2km;
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Time Charges</td>
                                                            <td> Rs.
                                                                <?php
                                                                $time_charges=$ridenow['time_charges'];
                                                                if($time_charges=="")
                                                                {
                                                                    echo "------";
                                                                }
                                                                else
                                                                {
                                                                    echo $time_charges;
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Waiting Charges</td>
                                                            <td> Rs.
                                                                <?php
                                                                $waiting_charges=$ridenow['waiting_charges'];
                                                                if($waiting_charges=="")
                                                                {
                                                                    echo "------";
                                                                }
                                                                else
                                                                {
                                                                    echo $waiting_charges;
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Extra Charges</td>
                                                            <td> Rs.
                                                                <?php
                                                                $extra_charges=$ridenow['extra_charges'];
                                                                if($extra_charges=="")
                                                                {
                                                                    echo "------";
                                                                }
                                                                else
                                                                {
                                                                    echo $extra_charges;
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Total Bill</strong></td>
                                                            <td><strong>Rs. <?php echo $basefare + $first_2km + $after_2km + $time_charges + $waiting_charges + $extra_charges; ?></strong></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>

                                                    <table class="table-bordered" align="left"  width="49.80%" cellspacing="0" cellpadding="0" style="border-left:0px; border-right:0px;">
                                                        <tbody>
                                                        <tr>
                                                            <td width="18%"><img src="<?php if($ridenow['driver_image'] != '' && isset($ridenow['driver_image'])){echo '../'.$ridenow['driver_image'];}else{ echo $dummyImg; }?>"  width="50px" height="50px"></td>
                                                            <td width="82%" style="vertical-align:middle;"><?php echo $ridenow['driver_name']; ?></td>
                                                        </tr>

                                                        <tr>
                                                            <td width="18%"><img src="http://apporio.co.uk/apporiotaxi/admin/images/distance.png" width="50" height="50"/></td>
                                                            <td width="82%" style="vertical-align:middle;"><span><?php echo $ridenow['distance']; ?> km</span> &nbsp;&nbsp; &nbsp; <span>
                  <?php $begin_time=$ridenow['begin_time']; if($begin_time==""){ echo "------";} else { echo $begin_time; } ?> To <?php $end_time=$ridenow['end_time']; if($end_time==""){ echo "------";} else { echo $end_time; } ?></span></td>
                                                        </tr>

                                                        <tr>
                                                            <td width="18%"><img src="http://apporio.co.uk/apporiotaxi/admin/images/car.png" width="50" height="50"/></td>
                                                            <td width="82%" style="vertical-align:middle;"><?php echo $ridenow['car_type_name']; ?></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="padding:0px">
                                        <table class="table-bordered" align="left"  width="100%" cellspacing="0" cellpadding="0" style="border:0px;">
                                            <tr>
                                                <td width="5%" style="vertical-align: middle;">
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                        <tr>
                                                            <td><img src="http://apporio.co.uk/apporiotaxi/admin/images/marker.png" width="6" height="77"/></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>

                                                <td width="95%" style="vertical-align: middle;">
                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                        <tr style="border-bottom: 1px solid #ddd;">
                                                            <td>
                                                                <?php
                                                                $begin_location=$ridenow['begin_location'];
                                                                if($begin_location=="")
                                                                {
                                                                    echo "------";
                                                                }
                                                                else
                                                                {
                                                                    echo $begin_location;
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <?php
                                                                $end_location=$ridenow['end_location'];
                                                                if($end_location=="")
                                                                {
                                                                    echo "------";
                                                                }
                                                                else
                                                                {
                                                                    echo $end_location;
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
















                            </table>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>

    </div>
    </div>
<?php }?>



</section>
</body>
</html>