<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['BID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');

$query="select * from department WHERE branch_id='".$_SESSION['ADMIN']['BID']."' ORDER BY department_id DESC";
$result = $db->query($query);
$list=$result->rows;

if(isset($_GET['status']) && isset($_GET['id']))
{
    $query1="UPDATE department SET department_status='".$_GET['status']."' WHERE department_id='".$_GET['id']."'";
    $db->query($query1);
    $db->redirect("home.php?pages=view_department");
}

if(isset($_POST['savechanges']))
{
    $query2="UPDATE department SET department_name ='".$_POST['department_name']."',department_email='".$_POST['department_email']."',department_phone='".$_POST['department_phone']."',department_address='".$_POST['department_address']."',department_head='".$_POST['department_head']."' where department_id='".$_POST['savechanges']."'";
    $db->query($query2);
    $db->redirect("home.php?pages=view_department");

}
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB21baMnSAvRrcZ3IKymum3_vJFN4hm7xU&v=3.exp&libraries=places"></script>

<script>
    function initialize() {

        var input = document.getElementById('department_address');

        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<form method="post" name="frm">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">View Department Details</h3>

        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Address</th>
                                        <th>Head</th>
                                        <th>Status</th>
                                        <th>Edit</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                    $i = 1;
                                    foreach($list as $department){?>
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td><?= $department['department_name'];?></td>
                                            <td><?= $department['department_email'];?></td>
                                            <td><?= $department['department_phone'];?></td>
                                            <td><?= $department['department_address'];?></td>
                                            <td><?= $department['department_head'];?></td>
                                            <?php
                                            if($department['department_status']==1) {
                                                ?>
                                                <td class="text-center">
                                                    <a href="home.php?pages=view_department&status=2&id=<?php echo $department['department_id']?>" class="" title="Active">
                                                        <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active
                                                        </button></a>
                                                </td>
                                                <?php
                                            } else {
                                                ?>
                                                <td class="text-center">
                                                    <a href="home.php?pages=view_department&status=1&id=<?php echo $department['department_id']?>" class="" title="Deactive">
                                                        <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive
                                                        </button></a>
                                                </td>
                                            <?php } ?>
                                            <td><button type="button" class="btn btn-info glyphicon glyphicon-pencil" data-toggle="modal" data-target="#<?php echo $department['department_id'];?>"  ></button></td>
                                        </tr>
                                        <?php $i++;
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End row -->

    </div>
</form>

<?php foreach($list as $department){?>
    <div class="modal fade" id="<?php echo $department['department_id'];?>" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Edit Department Details</h4>
                </div>
                <form  method="post" >
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Name</label>
                                    <input type="text" class="form-control"  placeholder="Department Name" name="department_name" value="<?php echo $department['department_name'];?>" id="department_name" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Email</label>
                                    <input type="text" class="form-control"  placeholder="Department Email" name="department_email" value="<?php echo $department['department_email'];?>" id="department_email" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Phone</label>
                                    <input type="text" class="form-control"  placeholder="Department Phone" name="department_phone" value="<?php echo $department['department_phone'];?>" id="department_phone" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Address</label>
                                    <input type="text" class="form-control"  placeholder="Department Address" name="department_address" value="<?php echo $department['department_address'];?>" id="department_address" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Head Person</label>
                                    <input type="text" class="form-control"  placeholder="Department Head" name="department_head" value="<?php echo $department['department_head'];?>" id="department_head" required>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" name="savechanges" value="<?php echo $department['department_id'];?>" class="btn btn-info">Save Changes</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
<?php }?>
<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>