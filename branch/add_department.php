<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['BID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');

if(isset($_POST['save']))
{
    $department_role = "";
    $query="select * from department WHERE department_email='".$_POST['department_email']."'";
    $result = $db->query($query);
    $list=$result->row;
    if(count($list) == 0)
    {
        $query2="INSERT INTO department (subdepartment_id,company_id,branch_id,department_name,department_email,department_phone,department_address,department_head,password,department_role)
 VALUES ('".$_POST['subdepartment_id']."','".$_SESSION['ADMIN']['BCID']."','".$_SESSION['ADMIN']['BID']."','".$_POST['department_name']."','".$_POST['department_email']."','".$_POST['department_phone']."','".$_POST['department_address']."','".$_POST['department_head']."','".$_POST['password']."','$department_role')";
        $db->query($query2);
    }else{
        $errorMsg = "Email already in Registerd!";
    }
}
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqjX8hLADF34kF5YMFZhoutxl9SVOamP0&v=3.exp&libraries=places"></script>

<script>
    function initialize() {

        var input = document.getElementById('department_address	');

        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>




<!-- Page Content Start -->
<!-- ================== -->

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Add Department Details</h3>
        <?php if(isset($errorMsg)){ ?>
            <h3 style="float:right; color:red;">Email already Registerd!</h3>
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form">
                        <form class="cmxform form-horizontal tasi-form"  method="post">

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Name</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control"  placeholder="Department Name" name="department_name" id="department_name" required>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Email</label>
                                <div class="col-lg-10">
                                    <input type="email" class="form-control"  placeholder="Department Email" name="department_email" id="department_email" required>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Phone</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control"  placeholder="Department Phone" name="department_phone" id="department_phone" required>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Department Head</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control"  placeholder="Department Head" name="department_head" id="department_head" required>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Address</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control"  placeholder="Department Address" name="department_address" id="department_address" required>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Default Password</label>
                                <div class="col-lg-10">
                                    <input type="password" class="form-control"  placeholder="Default Password" name="password" id="password" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- .form -->

                </div>
                <!-- panel-body -->
            </div>
            <!-- panel -->
        </div>
        <!-- col -->

    </div>
    <!-- End row -->

</div>

<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>
