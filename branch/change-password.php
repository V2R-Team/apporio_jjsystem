<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['BEMAIL'] == "")
{
    $db->redirect("home.php?pages=index");
}
$email = $_SESSION['ADMIN']['BEMAIL'];

if(isset($_REQUEST['submit']))
{
    $password = $_POST['password'];
    $db->query("UPDATE branches SET branch_password = '$password',branch_password_defult ='2' WHERE branch_email = '$email'");
    $query="select * from branches WHERE branch_email='$email'";
    $result = $db->query($query);
    $list=$result->row;
    $_SESSION['ADMIN']['BID'] = $list['branch_id'];
    $_SESSION['ADMIN']['BCID'] = $list['company_id'];
    $_SESSION['ADMIN']['BUN'] = $list['branch_name'];
    $_SESSION['ADMIN']['BROLE'] = $list['branch_role'];
    $_SESSION['ADMIN']['BEMAIL'] = $list['branch_email'];
    header("Location: home.php?pages=dashboard");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>JJ System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="../js/jquery.js"></script>
    <link href="../css/password.css" rel="stylesheet">
    <style type="text/css">
        .separator {
            border-right: 1px solid #dfdfe0;
        }
        .icon-btn-save {
            padding-top: 0;
            padding-bottom: 0;
        }
        .input-group {
            margin-bottom:10px;
        }
        .btn-save-label {
            position: relative;
            left: -12px;
            display: inline-block;
            padding: 6px 12px;
            background: rgba(0,0,0,0.15);
            border-radius: 3px 0 0 3px;
        }
    </style>
    <script type="text/javascript">
        function Validate() {
            var password = document.getElementById("password").value;
            var confirmPassword = document.getElementById("cpassword").value;
            if (password != confirmPassword) {
                alert("Password And Confirm Password d'not match!!");
                return false;
            }
            return true;
        }
    </script>
</head>
<body>

<div class="container bootstrap snippet">
    <form method="post">
    <div class="row" style="margin-top: 120px;">
        <div class="col-md-4"></div>
        <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon glyphicon-th"></span>
                        Change Your password
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div style="margin-top:40px;" class="col-md-12 login-box">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                                    <input class="form-control" type="password" name="password" id="password" placeholder="New Password" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                                    <input class="form-control" type="password" name="cpassword" id="cpassword" placeholder="Confirm Password" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6"></div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <button class="btn icon-btn-save btn-success" onclick="return Validate()" type="submit" name="submit">
                                <span class="btn-save-label"><i class="glyphicon glyphicon-floppy-disk"></i></span>save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
    </form>
</div>

<script src="../js/bootstrap.min.js"></script>
<script type="text/javascript">

</script>
</body>
</html>