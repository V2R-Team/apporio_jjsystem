<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Taxi Admin Panel">
            
        <title>JJsystem Admin</title>
        
        <!-- Stylesheet Starts -->
        <?php include('style.php'); ?>
        <!-- Stylesheet Closed -->

    </head>


    <body>

        <!-- Sidebar Menu Start-->
        <?php include('sidebar.php'); ?>
        <!-- Sidebar Menu Ends-->


        <!--Main Content Start -->
        <section class="content">
            
            <!-- Header Starts -->
            <?php include('header.php'); ?>
            <!-- Header Starts -->
            
            <!-- Footer Starts -->
            <?php include('footer.php'); ?>
            <!-- Footer Closed -->
            
            <!-- Javascript Starts -->
            <?php include('design.php'); ?>
            <!-- Javascript Starts -->
			
            