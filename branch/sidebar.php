<?php 
include_once '../apporioconfig/connection.php'; 

  $admin_url =  ADMIN_URL;
  $request = $_SERVER[REQUEST_URI];
 ?>
<aside class="left-panel"> 
  
  <!-- brand -->
  <div class="logo"> <a href="home.php?pages=dashboard" class="logo-expanded"> <img src="img/taxilogo.png" alt="logo"> <span class="nav-label">JJ System</span> </a> </div>
  <!-- / brand --> 
  
  <!-- Navbar Start -->
  <nav class="navigation">
    <ul class="list-unstyled">
      <!-- Dashboard Start -->
      <?php
        $li_open = $arr_open = $ul_open = "";
        if(empty($_REQUEST['pages'])) {
            $li_open    = "active open";
            $arr_open   = "open";
            $ul_open    = "style='display: block'";
        }
        ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=dashboard"><i class="glyphicon glyphicon-home" aria-hidden="true"></i> <span class="nav-label">Dashboard</span><span class="selected"></span></a></li>
<?php 
   $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
   if($actual_link == "$admin_url/dashboard.php"){
    header("Location: home.php?pages=dashboard");
   }
    if($actual_link == "$admin_url/view_employee.php"){
        header("Location: home.php?pages=dashboard");
    }
    if($actual_link == "$admin_url/add_employee.php"){
        header("Location: home.php?pages=dashboard");
    }
?>

      <?php if($_SESSION['ADMIN']['BROLE'] == ""){  ?>

        <!-- Add Branch Start -->
        <?php
        $li_open = $arr_open = $ul_open = "";
        if(@$_REQUEST['pages'] == "add_department" || $_REQUEST['pages'] == "view_department") {
            $li_open    = "active open";
            $arr_open   = "open";
            $ul_open    = "style='display: block'";
        }
        ?>
        <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-building"></i> <span class="nav-label">Department</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?> "></span></a>
            <ul class="list-unstyled">
                <li><a href="home.php?pages=add_department">Add Department</a></li>
                <li><a href="home.php?pages=view_department">View Department</a></li>
            </ul>
        </li>


      <?php
                            $li_open = $arr_open = $ul_open = "";
                            if(@$_REQUEST['pages'] == "ride-completed") {
                                $li_open    = "active open";
                                $arr_open   = "open";
                                $ul_open    = "style='display: block'";
                            }
                            ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=ride-completed"><i class="fa fa-taxi" aria-hidden="true"></i> <span class="nav-label" >Ride Completed</span><span class="selected"></span></a></li>


        <!-- Transactions Start -->
        <?php
        $li_open = $arr_open = $ul_open = "";
        if(@$_REQUEST['pages'] == "view-transactions") {
            $li_open    = "active open";
            $arr_open   = "open";
            $ul_open    = "style='display: block'";
        }
        ?>
        <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=view-transactions"><i class="fa fa-money" aria-hidden="true"></i> <span class="nav-label" >Transactions</span><span class="selected"></span></a></li>
 <!-- ride-->
        <?php
        $li_open = $arr_open = $ul_open = "";
        if(@$_REQUEST['pages'] == "ride-now" || $_REQUEST['pages'] == "ride-later") {
            $li_open    = "active open";
            $arr_open   = "open";
            $ul_open    = "style='display: block'";
        }
        ?>
        <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-taxi"></i> <span class="nav-label">Rides</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?> "></span></a>
            <ul class="list-unstyled">
                <li><a href="home.php?pages=ride-now">Ride Now</a></li>
                <li><a href="home.php?pages=ride-later">Ride Later</a></li>
            </ul>
        </li>

    <?php }else{  

        $query="select * from role WHERE role_id = '".$_SESSION['ADMIN']['CROLE']."' ";
        $result = $db->query($query);
        $list=$result->rows;
        foreach($list as $lists);
        $data =  json_decode(html_entity_decode($lists['role_permission']), true);
       
    ?>

        <?php if($data[add_company] == 1 OR $data[view_company] == 1){  ?>
            <!-- Add city model Start -->
            <?php
            $li_open = $arr_open = $ul_open = "";
            if(@$_REQUEST['pages'] == "add-company" || $_REQUEST['pages'] == "view-company") {
                $li_open    = "active open";
                $arr_open   = "open";
                $ul_open    = "style='display: block'";
            }
            ?>
            <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-road"></i> <span class="nav-label">Company</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?> "></span></a>
                <ul class="list-unstyled">

                    <?php if($data[add_company] == 1){  ?>
                        <li><a href="home.php?pages=add-company">Add Company</a></li>
                    <?php } if($data[view_company] == 1){  ?>
                        <li><a href="home.php?pages=view-company">View Company</a></li>
                    <?php } ?>
                </ul>
            </li>
        <?php }  ?>

      <?php if($data[role_add] == 1 OR $data[role_view] == 1  ){  ?>
      <!-- Add Role -->
      <?php
          $li_open = $arr_open = $ul_open = "";
          if(@$_REQUEST['pages'] == "add-role" || $_REQUEST['pages'] == "view-role") {
              $li_open    = "active open";
              $arr_open   = "open";
              $ul_open    = "style='display: block'";
          }
       ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-users" aria-hidden="true"></i> <span class="nav-label">Role</span><span class="selected"></span><span class="arrow <?php echo $arr_open ?>"></span></a>
        <ul class="list-unstyled">

        <?php if($data[role_add] == 1){  ?>
          <li><a href="home.php?pages=add-role">Add Role</a></li>
        <?php } if($data[role_view] == 1){  ?>
          <li><a href="home.php?pages=view-role">View Role</a></li>
        <?php } ?>

        </ul>
      </li>
      <?php }  ?>

      <?php if($data[subadmin_add] == 1 OR $data[subadmin_view] == 1 ){  ?>

      <!-- Add Sub Admin -->
      <?php
          $li_open = $arr_open = $ul_open = "";
          if(@$_REQUEST['pages'] == "add-subadmin" || $_REQUEST['pages'] == "view-subadmin") {
              $li_open    = "active open";
              $arr_open   = "open";
              $ul_open    = "style='display: block'";
          }
       ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-users" aria-hidden="true"></i> <span class="nav-label">Sub Admin</span><span class="selected"></span><span class="arrow <?php echo $arr_open ?>"></span></a>
        <ul class="list-unstyled">

        <?php if($data[subadmin_add] == 1){  ?>
          <li><a href="home.php?pages=add-subadmin">Add Sub Admin</a></li>
        <?php } if($data[subadmin_view] == 1){  ?>
          <li><a href="home.php?pages=view-subadmin">View Sub Admin</a></li>
        <?php } ?>

          
          
        </ul>
      </li>
      
       
    <!-- <li class="has-submenu"><a href="#"><i class="fa fa-bell"></i> <span class="nav-label">Push Notification</span></a></li>-->
    </ul>
    <ul class="list-unstyled">
    <?php }  ?>

      <?php if($data[account] == 1 ){  ?>
      <li><a href="home.php?pages=accounts"><i class="fa fa-user" aria-hidden="true"></i>Accounts</a></li>
      <?php }  ?>
    </ul>

    <?php  } ?>
  </nav>
</aside>
