<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['BID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');
$query="select * from done_ride order by done_ride_id desc";
$result = $db->query($query);
$listdoneride=$result->rows;

foreach($listdoneride as $login)
{
    $done_ride_id=$login['done_ride_id'];
    $query1="select * from payment_confirm where order_id='$done_ride_id'";
    $result1 = $db->query($query1);
    $ex_rows=$result1->num_rows;



    if ($ex_rows==0)
    {
        // Skip this part

        //echo "if chla";
    }
    else
    {
        //echo "else chla";
        $query123="select * from done_ride INNER JOIN payment_confirm ON done_ride.done_ride_id=payment_confirm.order_id where done_ride.done_ride_id='$done_ride_id' AND done_ride.branch_id='".$_SESSION['ADMIN']['BID']."' ";
        $result123 = $db->query($query123);
        $list12345=$result123->rows;

        foreach($list12345 as $login123)
//print_r ($list12345);
        {

            $begin_location=$login123['begin_location'];
            $end_location=$login123['end_location'];
            $order_id=$login123['order_id'];
            $payment_id=$login123['payment_id'];
            $payment_amount=$login123['payment_amount'];
            $payment_method=$login123['payment_method'];
            $payment_status=$login123['payment_status'];
            $payment_platform=$login123['payment_platform'];
            $payment_date_time=$login123['payment_date_time'];
            $arrived_time=$login123['arrived_time'];
            $begin_time=$login123['begin_time'];
            $end_time=$login123['end_time'];
            $waiting_time=$login123['waiting_time'];
            $distance=$login123['distance'];
            $tot_time=$login123['tot_time'];

            $c[]=array('begin_location'=>$begin_location,'end_location'=>$end_location,'order_id'=>$order_id,'payment_id'=>$payment_id,'payment_amount'=>$payment_amount,'payment_method'=>$payment_method, 'payment_status'=>$payment_status, 'payment_platform'=>$payment_platform, 'payment_date_time'=>$payment_date_time, 'arrived_time'=>$arrived_time, 'begin_time'=>$begin_time, 'end_time'=>$end_time, 'waiting_time'=>$waiting_time, 'distance'=>$distance, 'tot_time'=>$tot_time);
        }
    }

}

?>

<form method="post" name="frm">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">View Transactions</h3>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th width="12%">Request id</th>
                                        <th width="13%">Transaction id</th>
                                        <th>Pickup Address</th>
                                        <th>Drop Address</th>
                                        <th>Total Amount</th>
                                        <th>Payment Mode</th>
                                        <th width="8%"> Status</th>
                                        <th>Full Details</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($c as $payment_confirm){?>
                                        <tr>
                                            <td><?php
                                                $order_id=$payment_confirm['order_id'];
                                                if($order_id=="")
                                                {
                                                    echo "------";
                                                }
                                                else
                                                {
                                                    echo $order_id;
                                                }
                                                ?></td>
                                            <td><?php
                                                $payment_id=$payment_confirm['payment_id'];
                                                if($payment_id=="")
                                                {
                                                    echo "------";
                                                }
                                                else
                                                {
                                                    echo $payment_id;
                                                }
                                                ?></td>
                                            <td><?php
                                                $begin_location=$payment_confirm['begin_location'];
                                                if($begin_location=="")
                                                {
                                                    echo "------";
                                                }
                                                else
                                                {
                                                    echo $begin_location;
                                                }
                                                ?></td>
                                            <td><?php
                                                $end_location=$payment_confirm['end_location'];
                                                if($end_location=="")
                                                {
                                                    echo "------";
                                                }
                                                else
                                                {
                                                    echo $end_location;
                                                }
                                                ?></td>
                                            <td><?php
                                                $payment_amount=$payment_confirm['payment_amount'];
                                                if($payment_amount=="")
                                                {
                                                    echo "------";
                                                }
                                                else
                                                {
                                                    echo $payment_amount;
                                                }
                                                ?></td>
                                            <td><?php
                                                $payment_method=$payment_confirm['payment_method'];
                                                if($payment_method=="")
                                                {
                                                    echo "------";
                                                }
                                                else
                                                {
                                                    echo $payment_method;
                                                }
                                                ?></td>
                                            <td><?php
                                                $payment_status=$payment_confirm['payment_status'];
                                                if($payment_status=="")
                                                {
                                                    echo "------";
                                                }
                                                else
                                                {
                                                    echo $payment_status;
                                                }
                                                ?></td>
                                            <td><button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#details<?php echo $payment_confirm['order_id'];?>"  > Full Details </button></td>
                                        </tr>
                                    <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End row -->

    </div>
</form>
<?php foreach($c as $payment_confirm) { ?>

    <!-- Popup -->
    <div class="modal fade"  id="details<?php echo $payment_confirm['order_id'];?>" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Transaction Full Details</h4>
                    <div id="assign_driver_success" style="display:none;"> <br>
                        <div class="alert alert-succ$pages->driver_idess"> <strong>Success!</strong> Driver Assigned for Ride Id #<?php echo $ridelater['ride_id'];?> </div>
                    </div>
                </div>
                <div class="modal-body" >
                    <div class="tab-content">
                        <div id="booking_status_on" class="tab-pane fade in active" style="max-height: 400px; overflow-x: auto;">
                            <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                <thead>
                                <tr>
                                    <th width="40%">Request id</th>
                                    <td class=""><?php
                                        $order_id=$payment_confirm['order_id'];
                                        if($order_id=="")
                                        {
                                            echo "------";
                                        }
                                        else
                                        {
                                            echo $order_id;
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <th>Payment id</th>
                                    <td class=""><?php
                                        $payment_id=$payment_confirm['payment_id'];
                                        if($payment_id=="")
                                        {
                                            echo "------";
                                        }
                                        else
                                        {
                                            echo $payment_id;
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <th>Pickup Address</th>
                                    <td class=""><?php
                                        $begin_location=$payment_confirm['begin_location'];
                                        if($begin_location=="")
                                        {
                                            echo "------";
                                        }
                                        else
                                        {
                                            echo $begin_location;
                                        }
                                        ?></td>
                                <tr>
                                    <th>Drop Address</th>
                                    <td class=""><?php
                                        $end_location=$payment_confirm['end_location'];
                                        if($end_location=="")
                                        {
                                            echo "------";
                                        }
                                        else
                                        {
                                            echo $end_location;
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <th>Total Amount</th>
                                    <td><?php
                                        $payment_amount=$payment_confirm['payment_amount'];
                                        if($payment_amount=="")
                                        {
                                            echo "------";
                                        }
                                        else
                                        {
                                            echo $payment_amount;
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <th>Payment Mode</th>
                                    <td><?php
                                        $payment_method=$payment_confirm['payment_method'];
                                        if($payment_method=="")
                                        {
                                            echo "------";
                                        }
                                        else
                                        {
                                            echo $payment_method;
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <th>Payment Status</th>
                                    <td><?php
                                        $payment_status=$payment_confirm['payment_status'];
                                        if($payment_status=="")
                                        {
                                            echo "------";
                                        }
                                        else
                                        {
                                            echo $payment_status;
                                        }
                                        ?></td>
                                </tr>

                                <tr>
                                    <th>Payment Platform</th>
                                    <td><?php
                                        $payment_platform=$payment_confirm['payment_platform'];
                                        if($payment_platform=="")
                                        {
                                            echo "------";
                                        }
                                        else
                                        {
                                            echo $payment_platform;
                                        }
                                        ?></td>
                                </tr>

                                <tr>
                                    <th>Payment Date</th>
                                    <td><?php
                                        $payment_date_time=$payment_confirm['payment_date_time'];
                                        if($payment_date_time=="")
                                        {
                                            echo "------";
                                        }
                                        else
                                        {
                                            echo $payment_date_time;
                                        }
                                        ?></td>
                                </tr>

                                <tr>
                                    <th>Payment Status</th>
                                    <td><?php
                                        $payment_status=$payment_confirm['payment_status'];
                                        if($payment_status=="")
                                        {
                                            echo "------";
                                        }
                                        else
                                        {
                                            echo $payment_status;
                                        }
                                        ?></td>
                                </tr>

                                <tr>
                                    <th>Arrived Time</th>
                                    <td><?php
                                        $arrived_time=$payment_confirm['arrived_time'];
                                        if($arrived_time=="")
                                        {
                                            echo "------";
                                        }
                                        else
                                        {
                                            echo $arrived_time;
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <th>Start Time</th>
                                    <td><?php
                                        $begin_time=$payment_confirm['begin_time'];
                                        if($begin_time=="")
                                        {
                                            echo "------";
                                        }
                                        else
                                        {
                                            echo $begin_time;
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <th>End Time</th>
                                    <td><?php
                                        $end_time=$payment_confirm['end_time'];
                                        if($end_time=="")
                                        {
                                            echo "------";
                                        }
                                        else
                                        {
                                            echo $end_time;
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <th>Waiting Time</th>
                                    <td><?php
                                        $waiting_time=$payment_confirm['waiting_time'];
                                        if($waiting_time=="")
                                        {
                                            echo "------";
                                        }
                                        else
                                        {
                                            echo $waiting_time;
                                        }
                                        ?></td>
                                </tr>

                                <tr>
                                    <th>Ride Time</th>
                                    <td><?php
                                        $ride_time=$payment_confirm['ride_time'];
                                        if($ride_time=="")
                                        {
                                            echo "------";
                                        }
                                        else
                                        {
                                            echo $ride_time;
                                        }
                                        ?></td>
                                </tr>

                                <tr>
                                    <th>Distance</th>
                                    <td><?php
                                        $distance=$payment_confirm['distance'];
                                        if($distance=="")
                                        {
                                            echo "------";
                                        }
                                        else
                                        {
                                            echo $distance;
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <th>Total Time</th>
                                    <td><?php
                                        $tot_time=$payment_confirm['tot_time'];
                                        if($tot_time=="")
                                        {
                                            echo "------";
                                        }
                                        else
                                        {
                                            echo $tot_time;
                                        }
                                        ?></td>
                                </tr>

                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>