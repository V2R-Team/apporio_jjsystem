<!DOCTYPE html>
<html lang="en">


<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Taxi Admin">
<meta name="author" content="">

<title>JJ System </title>

<!-- Google-Fonts -->
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic' rel='stylesheet'>


<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-reset.css" rel="stylesheet">

<!--Animation css-->
<link href="css/animate.css" rel="stylesheet">

<!--Icon-fonts css-->
<link href="taxi/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="taxi/ionicon/css/ionicons.min.css" rel="stylesheet" />

<!--Morris Chart CSS -->

<!-- Custom styles for this template -->
<link href="css/style.css" rel="stylesheet">
<link href="css/helper.css" rel="stylesheet">
<link href="css/style-responsive.css" rel="stylesheet" />

<!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->


</head>


<body>

<div class="wrapper-page animated fadeInDown">
    <div class="panel panel-color panel-primary">
            <div class="form-group ">
            <div class="col-xs-12">
                <a href="http://apporio.org/jjsystem/jjsystem/" style="margin-bottom: 30px" class="btn btn-primary btn-lg btn-block" role="button">JJ System Admin</a>
            </div>
        </div>
        <div class="form-group ">
            <div class="col-xs-12">
                <a href="http://apporio.org/jjsystem/company/" style="margin-bottom: 30px" role="button" class="btn btn-primary btn-lg btn-block">Company Admin</a>
            </div>
        </div>
        <div class="form-group ">
            <div class="col-xs-12">
                <a href="http://apporio.org/jjsystem/branch/" role="button" style="margin-bottom: 30px" class="btn btn-primary btn-lg btn-block">Branch Admin</a>
            </div>
        </div>
        <div class="form-group ">
            <div class="col-xs-12">
                <a href="http://apporio.org/jjsystem/department/" role="button"  class="btn btn-primary btn-lg btn-block">Department Admin</a>
            </div>
        </div>

        <div class="clear"></div>

    </div>
</div>


<!-- js placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/pace.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.nicescroll.js" type="text/javascript"></script>


<!--common script for all pages-->
<script src="js/jquery.app.js"></script>


</body>

</html>
