<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['CID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');

if(isset($_POST['save']))
{
    $subbranch_id = "";
    $branch_role = "";
    $query="select * from branches WHERE branch_email='".$_POST['branch_email']."'";
    $result = $db->query($query);
    $list=$result->row;
    if(count($list) == 0)
    {
        $query2="INSERT INTO branches (subbranch_id,company_id,branch_name,branch_email,branch_phone,branch_address,branch_contact_person,branch_password,branch_role)
 VALUES ('".$_POST['subbranch_id']."','".$_SESSION['ADMIN']['CID']."','".$_POST['branch_name']."','".$_POST['branch_email']."','".$_POST['branch_phone']."','".$_POST['branch_address']."','".$_POST['branch_contact_person']."','".$_POST['branch_password']."','$branch_role')";
        $db->query($query2);

    }else{
        $errorMsg = "Email already in Registerd!";
    }
}
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB21baMnSAvRrcZ3IKymum3_vJFN4hm7xU&v=3.exp&libraries=places"></script>

<script>
    function initialize() {

        var input = document.getElementById('branch_address');

        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>




<!-- Page Content Start -->
<!-- ================== -->

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Add Branch Details</h3>
        <?php if(isset($errorMsg)){ ?>
            <h3 style="float:right; color:red;">Email already Registerd!</h3>
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form">
                        <form class="cmxform form-horizontal tasi-form"  method="post">

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Name</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control"  placeholder="Branch Name" name="branch_name" id="branch_name" required>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Email</label>
                                <div class="col-lg-10">
                                    <input type="email" class="form-control"  placeholder="Branch Email" name="branch_email" id="branch_email" required>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Phone</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control"  placeholder="Branch Phone" name="branch_phone" id="branch_phone" required>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Contact Person</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control"  placeholder="Branch Contact Person" name="branch_contact_person" id="branch_contact_person" required>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Address</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control"  placeholder="Branch Address" name="branch_address" id="branch_address" required>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Default Password</label>
                                <div class="col-lg-10">
                                    <input type="password" class="form-control"  placeholder="Default Password" name="branch_password" id="branch_password" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- .form -->

                </div>
                <!-- panel-body -->
            </div>
            <!-- panel -->
        </div>
        <!-- col -->

    </div>
    <!-- End row -->

</div>

<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>
