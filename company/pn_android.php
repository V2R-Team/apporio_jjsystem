<?php
function AndroidPushNotificationCustomer($did, $msg,$ride_id,$ride_status) 
{
	// Set POST variables
	$url = 'https://fcm.googleapis.com/fcm/send';
	
	$app_id="1";
	
	$fields = array ('to' => $did,'data' => array('message' => $msg,'ride_id'=>$ride_id,'ride_status'=> $ride_status,'app_id'=>$app_id) );

	$headers = array (
			'Authorization: key=AAAAH-OLTQc:APA91bFvIjlPWWNLN6yeH6CXMuF5sEaS2RIiBWy1Txe6IJvy7EzMummDKxup4Wi9To-2fZRmWIUu3Mi-o6MXYJQ-F6Xf2GfA_rp_d5HJPRNA3FmGzPIjob0B7OARVW8q9xfO-tCx_wwv',
			'Content-Type: application/json' );

	// Open connection
	$ch = curl_init ();
	// Set the url, number of POST vars, POST data
	curl_setopt ( $ch, CURLOPT_URL, $url );
	curl_setopt ( $ch, CURLOPT_POST, true );
	curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
	curl_setopt ( $ch, CURLOPT_POSTFIELDS,  json_encode ( $fields ) );
	// Execute post
 	$result = curl_exec ( $ch );
	// Close connection
	curl_close ( $ch );
		return $result;
}

function AndroidPushNotificationDriver($did, $msg,$ride_id,$ride_status) {
	// Set POST variables
	$url = 'https://fcm.googleapis.com/fcm/send';
	
	$app_id="2";
	
	
	
	$fields = array ('to' => $did,'data' => array('message' => $msg,'ride_id'=>$ride_id,'ride_status'=> $ride_status,'app_id'=>$app_id) );

	$headers = array (
			'Authorization: key=AAAAH-OLTQc:APA91bFvIjlPWWNLN6yeH6CXMuF5sEaS2RIiBWy1Txe6IJvy7EzMummDKxup4Wi9To-2fZRmWIUu3Mi-o6MXYJQ-F6Xf2GfA_rp_d5HJPRNA3FmGzPIjob0B7OARVW8q9xfO-tCx_wwv',
			'Content-Type: application/json' );
	// Open connection
	$ch = curl_init ();
	// Set the url, number of POST vars, POST data
	curl_setopt ( $ch, CURLOPT_URL, $url );
	curl_setopt ( $ch, CURLOPT_POST, true );
	curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
	curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
	// Execute post
	$result = curl_exec ( $ch );
	// Close connection
	curl_close ( $ch );
        return $result;
}
?>