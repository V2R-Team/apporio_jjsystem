<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['CID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');

$query="select * from branches WHERE company_id='".$_SESSION['ADMIN']['CID']."' ORDER BY branch_id DESC";
$result = $db->query($query);
$list=$result->rows;

if(isset($_GET['status']) && isset($_GET['id']))
{
    $query1="UPDATE branches SET branch_status='".$_GET['status']."' WHERE branch_id='".$_GET['id']."'";
    $db->query($query1);
    $db->redirect("home.php?pages=view_branch");
}

if(isset($_POST['savechanges']))
{
    $query2="UPDATE branches SET branch_name='".$_POST['branch_name']."',branch_email='".$_POST['branch_email']."',branch_phone='".$_POST['branch_phone']."',branch_address='".$_POST['branch_address']."',branch_contact_person='".$_POST['branch_contact_person']."' where branch_id='".$_POST['savechanges']."'";
    $db->query($query2);
    $db->redirect("home.php?pages=view_branch");

}
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB21baMnSAvRrcZ3IKymum3_vJFN4hm7xU&v=3.exp&libraries=places"></script>

<script>
    function initialize() {

        var input = document.getElementById('branch_address');

        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<form method="post" name="frm">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">View Branch Details</h3>

        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Branch Name</th>
                                        <th>Branch Email</th>
                                        <th>Branch Phone</th>
                                        <th>Branch Address</th>
                                        <th>Branch Contact Person</th>
                                        <th>Status</th>
                                        <th>Edit</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                    $i = 1;
                                    foreach($list as $branch){?>
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td><?= $branch['branch_name'];?></td>
                                            <td><?= $branch['branch_email'];?></td>
                                            <td><?= $branch['branch_phone'];?></td>
                                            <td><?= $branch['branch_address'];?></td>
                                            <td><?= $branch['branch_contact_person'];?></td>
                                            <?php
                                            if($branch['branch_status']==1) {
                                                ?>
                                                <td class="text-center">
                                                    <a href="home.php?pages=view_branch&status=2&id=<?php echo $branch['branch_id']?>" class="" title="Active">
                                                        <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active
                                                        </button></a>
                                                </td>
                                                <?php
                                            } else {
                                                ?>
                                                <td class="text-center">
                                                    <a href="home.php?pages=view_branch&status=1&id=<?php echo $branch['branch_id']?>" class="" title="Deactive">
                                                        <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive
                                                        </button></a>
                                                </td>
                                            <?php } ?>
                                            <td><button type="button" class="btn btn-info glyphicon glyphicon-pencil" data-toggle="modal" data-target="#<?php echo $branch['branch_id'];?>"  ></button></td>
                                        </tr>
                                        <?php $i++;
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End row -->

    </div>
</form>

<?php foreach($list as $branch){?>
    <div class="modal fade" id="<?php echo $branch['branch_id'];?>" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Edit Branch Details</h4>
                </div>
                <form  method="post" >
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Branch Name</label>
                                    <input type="text" class="form-control"  placeholder="Branch Name" name="branch_name" value="<?php echo $branch['branch_name'];?>" id="branch_name" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Branch Email</label>
                                    <input type="text" class="form-control"  placeholder="Branch Email" name="branch_email" value="<?php echo $branch['branch_email'];?>" id="branch_email" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Branch Phone</label>
                                    <input type="text" class="form-control"  placeholder="Branch Phone" name="branch_phone" value="<?php echo $branch['branch_phone'];?>" id="branch_phone" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Branch Address</label>
                                    <input type="text" class="form-control"  placeholder="Branch Address" name="branch_address" value="<?php echo $branch['branch_address'];?>" id="branch_address" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Branch Contact Person</label>
                                    <input type="text" class="form-control"  placeholder="Branch Contact Person" name="branch_contact_person" value="<?php echo $branch['branch_contact_person'];?>" id="branch_contact_person" required>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" name="savechanges" value="<?php echo $branch['branch_id'];?>" class="btn btn-info">Save Changes</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
<?php }?>
<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>