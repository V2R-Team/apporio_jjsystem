<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');

$query="select * from city ORDER BY city_id";
	$result = $db->query($query);
	$list=$result->rows;
        
        	
    if(isset($_GET['status']) && isset($_GET['id'])) 
    {
     $query1="UPDATE city SET status='".$_GET['status']."' WHERE city_id='".$_GET['id']."'";
     $db->query($query1);
     $db->redirect("home.php?pages=view-city");
    }

if(isset($_POST['savechanges'])) 
     {
       $query2="UPDATE city SET city_name='".$_POST['city_name']."' where city_id='".$_POST['savechanges']."'";
       $db->query($query2); 
       $db->redirect("home.php?pages=view-city");
     }		
    
?>

<form method="post" name="frm">
<div class="wraper container-fluid">
  <div class="page-title">
    <h3 class="title">View City</h3>
      </div>
  
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
              <table id="datatable" class="table table-striped table-bordered table-responsive">
                <thead>
                  <tr>
                    <th width="5%">S.No</th>
                    <th>City Name</th>
                    <th width="8%">Status</th>
                    <th width="4%">Edit</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($list as $city){?>
                  <tr>
                    <td><?php echo $city['city_id'];?></td>
                    <td>
                      <?php
                    	$city_name=$city['city_name'];
			if($city_name=="")
			{
				echo "------";
			}
			else
			{
				echo $city_name;
			}
                      ?>
                    </td>
                    <?php
                                if($city['status']==1) {
                                ?>
                                <td class="text-center">
                                    <a href="home.php?pages=view-city&status=2&id=<?php echo $city['city_id']?>" class="" title="Active">
                                    <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active
                                    </button></a>
                                </td>
                                <?php
                                } else {
                                ?>
                                <td class="text-center">
                                <a href="home.php?pages=view-city&status=1&id=<?php echo $city['city_id']?>" class="" title="Deactive">
                                    <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive
                                    </button></a>
                                </td>
                            <?php } ?>
 <td><button type="button" class="btn btn-info glyphicon glyphicon-pencil" data-toggle="modal" data-target="#<?php echo $city['city_id'];?>"  ></button></td>
                  </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End row --> 
  
</div>
</form>

<?php foreach($list as $city){?>
<div class="modal fade" id="<?php echo $city['city_id'];?>" role="dialog">
  <div class="modal-dialog"> 
    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title fdetailsheading">Edit City Details</h4>
      </div>
      <form  method="post"  onSubmit="return validatelogin()">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="field-3" class="control-label">City Name</label>
                <input type="text" class="form-control"  placeholder="City Name" name="city_name" value="<?php echo $city['city_name'];?>" id="city_name" required>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
          <button type="submit" name="savechanges" value="<?php echo $city['city_id'];?>" class="btn btn-info">Save Changes</button>
        </div>
      </form>
    </div>
    
  </div>
</div>
<?php }?>
<!-- Page Content Ends --> 
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>