<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');
?>

  <!-- Page Content Start --> 
  <!-- ================== -->
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){
    $("select").change(function(){
        $(this).find("option:selected").each(function(){
            if($(this).attr("value")=="overall"){
               
                $(".red").show();
                
                $(".green").hide();
            }
            else if($(this).attr("value")=="package"){
                
                $(".green").show();
                
                $(".red").hide();
            }
           
            else{
                $(".box").hide();
            }
        });
    }).change();
});
</script>
<style>
.total{ display:none;}
 
 .result{
        z-index: 999;
        top: 100%;
        left: 0;
        border-left:1px solid #f5f5f5 !important;
        border-right:1px solid #f5f5f5 !important;
        position: absolute;
        width: 97.6%;
        margin-left: 10px;
        background: #fff;
    }
    
    .result p{
        border-bottom:1px solid #f5f5f5 !important;
        padding:10px 0px 10px 10px;
        margin:0px !important;
    }
.result1{
        z-index: 999;
        top: 100%;
        left: 0;
        border-left:1px solid #f5f5f5 !important;
        border-right:1px solid #f5f5f5 !important;
        position: absolute;
        width: 97.6%;
        margin-left: 10px;
        background: #fff;
    }
    
    .result1 p{
        border-bottom:1px solid #f5f5f5 !important;
        padding:10px 0px 10px 10px;
        margin:0px !important;
    }
 
 </style> 


<script type="text/javascript">
$(document).ready(function(){
    $('#user_phone').on("keyup input", function(){
        var term = $(this).val();
        var resultDropdown = $(this).siblings(".result1");
        if(term.length){
            $.get("serach_phone.php", {query: term}).done(function(data){
         
                resultDropdown.html(data);
            });
        } else{
            resultDropdown.empty();
        }
    });
    $(document).on("click", ".result1 p", function(){
        $(this).parents(".green").find('input[type="text"]').val($(this).text());
        $(this).parent(".result1").empty();
    });
});
</script>
<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Book Ride</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          
          <div class="panel-body">
            <div class="form" >
              <form class="cmxform form-horizontal tasi-form"  method="post" action="home.php?pages=booking-form">              
              <div class="form-group">
                 <label class="control-label col-lg-2">Select User</label>
                 <div class="col-md-10">
                 <select class="form-control" name="quantity" id="quantity"  required>
                    <option value="">Search By Email or Phone</option>
                    <option value="overall">Email</option>
                    <option value="package">Phone</option>
                 </select>
                 </div>
               </div> 

              <div class="form-group total red">
                  <label class="control-label col-lg-2">User Email*</label>
                    <div class="col-lg-10">
                     <input type="text" class="form-control" placeholder="User Email" name="user_email" id="user_email" >
                    <div class="result"></div>
                  </div>
              </div>
                
              <div class="form-group total green">
                  <label class="control-label col-lg-2">User Phone</label>
                    <div class="col-lg-10">
                     <input type="text" class="form-control" placeholder="User Phone" name="user_phone" id="user_phone">
                     <div class="result1"></div>
                  </div>
              </div>
                  
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                   
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="submit" name="submit" value="Sarech" >
                  </div>
                </div>
              </form>
            </div>
            <!-- .form --> 
            
          </div>
          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
      
    </div>
    <!-- End row --> 
    
  </div>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#user_email').on("keyup input", function(){
        var term = $(this).val();
        var resultDropdown = $(this).siblings(".result");
        if(term.length){
            $.get("serach_user.php", {query: term}).done(function(data){
              
                resultDropdown.html(data);
            });
        } else{
            resultDropdown.empty();
        }
    });
    $(document).on("click", ".result p", function(){
        $(this).parents(".total").find('input[type="text"]').val($(this).text());
        $(this).parent(".result").empty();
    });
});
</script>

</section>
<!-- Main Content Ends -->

</body>
</html>
