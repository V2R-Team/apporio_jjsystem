<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');

    $query="select * from driver ORDER BY driver_id DESC";
	$result = $db->query($query);
	$list=$result->rows;
		
	$query12345="select * from car_type";
	$result12345 = $db->query($query12345);
	$list12345=$result12345->rows;  
	
	$query1234="select * from city";
	$result1234 = $db->query($query1234);
	$city=$result1234->rows;	
		
    if(isset($_GET['status']) && isset($_GET['id'])) 
    {
     $query1="UPDATE driver SET status='".$_GET['status']."' WHERE driver_id='".$_GET['id']."'";
     $db->query($query1);
     $db->redirect("home.php?pages=view-driver");
    }
    
    if(isset($_GET['online_offline']) && isset($_GET['id'])) 
    {
     $query1="UPDATE driver SET online_offline='".$_GET['online_offline']."' WHERE driver_id='".$_GET['id']."'";
     $db->query($query1);
     $db->redirect("home.php?pages=view-driver");
    }

if(isset($_POST['savechanges'])) 
     {
       $query2="UPDATE driver SET driver_name='".$_POST['driver_name']."', driver_email='".$_POST['driver_email']."',driver_phone='".$_POST['driver_phone']."',car_type_id='".$_POST['car_type_id']."',car_model_id='".$_POST['car_model_id']."',car_number='".$_POST['car_number']."',city_id='".$_POST['city_id']."' where driver_id='".$_POST['savechanges']."'";
       $db->query($query2); 
       $db->redirect("home.php?pages=view-driver");
     }			
      
        
?>



<form method="post" name="frm">
<input type="hidden" name="command" value="">
  <div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Drivers</h3>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                <table id="datatable" class="table table-striped table-bordered table-responsive">
                  <thead>
                    <tr>
                      <th width="5%">S.No</th>
                      <th>Driver Name</th>
                      <th>Email Id</th>
                      <th>Mobile No.</th>
                      <th>Rating</th>
                      <th>Status</th>
                      <th width="8%">Status</th>
                      <th width="12%">Full Details</th>
                      <th width="4%">Edit</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($list as $driver){?>
                    <tr>
                      <td><?php echo $driver['driver_id'];?></td>
                      <td>
					  	<?php
							$driver_name=$driver['driver_name'];
							if($driver_name=="")
							{
								echo "------";
							}
							else
							{
								echo $driver_name;
							}
						?>
                      </td>
                      <td>
					  	<?php
							$driver_email=$driver['driver_email'];
							if($driver_email=="")
							{
								echo "------";
							}
							else
							{
								$e1=substr($driver_email,0,2);
$e2= explode("@",$driver_email);
$domain=$e2[1];
								echo $e1."*****@".$domain;
							}
						?>
                      </td>
                      <td>
					  	<?php
							$driver_phone=$driver['driver_phone'];
							if($driver_phone=="")
							{
								echo "------";
							}
							else
							{
								$e1=substr($driver_phone,7);
								echo "********".$e1;
							}
						?>
                      </td>
                      <td><?php
   $driverrating=$driver['rating'];
   if($driverrating=="")
   {
     echo "<i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($driverrating==1)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($driverrating==2)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($driverrating==3)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($driverrating==4)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($driverrating==5)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i>";
   }
   else if($driverrating<1.5 && $driverrating>0 && $driverrating<1)
   {
     echo "<i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   
    else if($driverrating>1 && $driverrating<2)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   
    else if($driverrating>2 && $driverrating<3)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   
    else if($driverrating>3 && $driverrating<4)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i></i>";
   }
     else if($driverrating>4 && $driverrating<5)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i>";
   }
   
?></td>
                      <?php
                                if($driver['online_offline']==1) {
                                ?>
                      <td class="text-center"><a href="home.php?pages=view-driver&online_offline=2&id=<?php echo $driver['driver_id']?>" class="" title="Active">
                        <button type="button" class="btn btn-success br2 btn-xs fs12" > Online </button>
                        </a></td>
                      <?php
                                } else {
                                ?>
                      <td class="text-center"><a href="home.php?pages=view-driver&online_offline=1&id=<?php echo $driver['driver_id']?>" class="" title="Deactive">
                        <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Offline </button>
                        </a></td>
                      <?php } ?>
                      <?php
                                if($driver['status']==1) {
                                ?>
                      <td class="text-center"><a href="home.php?pages=view-driver&status=2&id=<?php echo $driver['driver_id']?>" class="" title="Active">
                        <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active </button>
                        </a></td>
                      <?php
                                } else {
                                ?>
                      <td class="text-center"><a href="home.php?pages=view-driver&status=1&id=<?php echo $driver['driver_id']?>" class="" title="Deactive">
                        <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive </button>
                        </a></td>
                      <?php } ?>
                      <td><button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#driverdetails<?php echo $driver['driver_id'];?>"  > Full Details </button></td>
 <td><button type="button" class="btn btn-info glyphicon glyphicon-pencil" data-toggle="modal" data-target="#edit<?php echo $driver['driver_id'];?>"  ></button></td>
                    </tr>
                    <?php }?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End row --> 
    
  </div>
</form>

<?php foreach($list as $driver){?>
<div class="modal fade" id="edit<?php echo $driver['driver_id'];?>" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content starts-->
    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title fdetailsheading">Edit Driver Details</h4>
      </div>
      <form  method="post"  onSubmit="return validatelogin()">
      <div class="modal-body">
        <div class="row">
		
          <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">Driver Name</label>
              <input type="text" class="form-control"  placeholder="Driver Name" name="driver_name" value="<?php echo $driver['driver_name'];?>" id="driver_name" required>
            </div>
          </div>
          
          <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">Driver Email</label>
              <input type="text" class="form-control"  placeholder="Driver Email" name="driver_email" value="<?php echo $driver['driver_email'];?>" id="driver_email" required>
            </div>
          </div>
          
	<div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">Driver Phone</label>
              <input type="text" class="form-control"  placeholder="Driver Phone" name="driver_phone" value="<?php echo $driver['driver_phone'];?>" id="driver_phone" required>
            </div>
          </div>
		  
         <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">Car Type</label>
              <select class="form-control" name="car_type_id" id="car_type_id"  onchange="getId(this.value);" required>
              
              <?php
                          	$car_type_id=$driver['car_type_id'];

			        $q="select * from car_type where car_type_id=$car_type_id";
				$r = $db->query($q);
				$l=$r->row;    


              			$car_type_name=$l['car_type_name'];
              ?>
              
                 <option value="<?php echo $car_type_id;?>"><?php echo $car_type_name; ?></option>
            <?php foreach($list12345 as $car) :?>
                           <option value="<?php echo $car['car_type_id'];?>"><?php echo $car['car_type_name']; ?></option>
                <?php endforeach; ?>
    					
					</select>
            </div>
          </div>
          
          <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">Car Model</label>
              <select class="form-control" name="car_model_id" id="car_model_id" required>
              <?php
                          	$car_model_id=$driver['car_model_id'];
			        $qu="select * from car_model where car_model_id=$car_model_id";
				$re = $db->query($qu);
				$li=$re->row;    


              			$car_model_name=$li['car_model_name'];
              ?>
              
                 <option value="<?php echo $car_model_id;?>"><?php echo $car_model_name; ?></option>
					</select>
            </div>
          </div>
		  
	 <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">Car Number</label>
              <input type="text" class="form-control"  placeholder="Car Number" name="car_number" value="<?php echo $driver['car_number'];?>" id="car_number" required>
            </div>
          </div>
          
           <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">City</label>
              <select class="form-control" name="city_id" id="city_id" required>
              <?php
                          	$city_id=$driver['city_id'];
			        $que="select * from city where city_id=$city_id";
				$res = $db->query($que);
				$lis=$res->row;    


              			$city_name=$lis['city_name'];
              ?>
              
                 <option value="<?php echo $city_id;?>"><?php echo $city_name; ?></option>
                  <?php foreach($city as $city) :?>
                           <option value="<?php echo $city['city_id'];?>"><?php echo $city['city_name']; ?></option>
                <?php endforeach; ?>
					</select>
            </div>
          </div>	  
		  
	    
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <button type="submit" name="savechanges" value="<?php echo $driver['driver_id'];?>" class="btn btn-info">Save Changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?php }?>



<!--Driver Details Starts-->
<?php
$dummyImg="http://apporio.co.uk/apporiotaxi/uploads/driver/driverprofile.png";
 foreach($list as $driver){?>
<div class="modal fade" id="driverdetails<?php echo $driver['driver_id'];?>" role="dialog"> <?php echo $driver['driver_id'];?>
  <div class="modal-dialog"> 
    
    <!-- Modal content starts-->
    
    <div class="modal-content" style="padding:20px !important;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title fdetailsheading">Driver Full Details</h4>
      </div>
        <div class="modal-body" style="max-height: 500px; overflow-x: auto;">
          <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
            <thead>
              <tr>
                <td class="" colspan="" style="text-align-last:left; width:150px;"><img src="<?php if($driver['driver_image'] != '' && isset($driver['driver_image'])){echo '../'.$driver['driver_image'];}else{ echo $dummyImg; }?>"  width="150px" height="150px"></td>
                <td><table style="margin-left:20px;" aling="center" border="0">
                    <tbody>
                      <tr>
                        <th class=""> Name</th>
                        <td class=""><?php
                             $drivername=$driver['driver_name'];
                             if($drivername=="")
                             {
                             echo "---------";
                             }
                             else
                             {
                             echo $drivername;
                             }
                           ?></td>
                      </tr>
                      <tr>
                        <th class="">Email</th>
                        <td class=""><?php
                             $driveremail=$driver['driver_email'];
                             if($driveremail=="")
                             {
                             echo "---------";
                             }
                             else
                             {
                             echo $driveremail;
                             }
                           ?></td>
                      </tr>
                      <tr>
                        <th class="">Phone</th>
                        <td class=""><?php
                             $driverphone=$driver['driver_phone'];
                             if($driverphone=="")
                             {
                             echo "---------";
                             }
                             else
                             {
                             echo $driverphone;
                             }
                           ?></td>
                      </tr>
                      <tr>
                        <th class="">Status</th>
                        <td class=""><?php
            	           	 $loginlogout=$driver['login_logout'];
            	      		 if($loginlogout==1)
            	      		 {
            	      		 echo "<button class=\"btn btn-success btn-xs activebtn\">Login</button>";
							 }
            	      		 else
            	      		 {
            	      		 echo "<button class=\"btn btn-danger btn-xs\">Logout</button>";
            	      		 }
            	   		  ?>
                          <?php
            	      	     $statusbusy=$driver['busy'];
            	      		 if($statusbusy==0)
            	      		 {
            	      		 echo "<button class=\"btn btn-success btn-xs activebtn\">Free</button>";
            	      		 }
            	      		 else if($statusbusy==1)
            	      		 {
            	      		 echo "<button class=\"btn btn-danger btn-xs\" >Busy</button>";
            	      		 }
            	      		  else
            	      		 {
            	      		 echo "----";
            	      		 }
            	   		  ?>
                          <?php
            	      		 $onlineoffline=$driver['online_offline'];
            	      		 if($onlineoffline==1)
            	             { 
            	      		 echo "<button class=\"btn btn-success btn-xs activebtn\">Online</button>";
            	      		 }
            	      		 else
            	     		 {
            	      		 echo "<button class=\"btn btn-danger btn-xs\">Offline</button>";
            	      		 }
            	   		 ?>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
              <tr>
                <th class="">Device</th>
                <td class=""><?php
                   	  $phonedevice=$driver['flag'];
                      if($phonedevice==1)
                      {
                      echo "Iphone";
                      }
                      else if($phonedevice==2)
                      {
                      echo "Android";
                      }
                      else
                      {
                      echo "---------";
                      }
                   ?></td>
              </tr>
              <tr>
                <th class="">Rating</th>
                <td class=""><?php
   $driverrating=$driver['rating'];
   if($driverrating=="")
   {
     echo "<i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($driverrating==1)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($driverrating==2)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($driverrating==3)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($driverrating==4)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($driverrating==5)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i>";
   }
   else if($driverrating<1.5 && $driverrating>0 && $driverrating<1)
   {
     echo "<i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   
    else if($driverrating>1 && $driverrating<2)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   
    else if($driverrating>2 && $driverrating<3)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   
    else if($driverrating>3 && $driverrating<4)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i></i>";
   }
     else if($driverrating>4 && $driverrating<5)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i>";
   }
   
?></td>
              </tr>
              <tr>
                <th class="">Car Type</th>
                <td class=""><?php 

$car_type_id=$driver['car_type_id'];

$query="select * from car_type where car_type_id=$car_type_id";
	$result = $db->query($query);
	$list=$result->row;    


$car_type_name=$list['car_type_name'];
if($car_type_name=="")
   {
   echo "---------";
   }
    else
    {
    echo $car_type_name;
    }
?></td>
              </tr>
              <tr>
                <th class="">Car Model</th>
                <td class=""><?php 

$car_model_id=$driver['car_model_id'];

$query="select * from car_model where car_model_id=$car_model_id";
	$result = $db->query($query);
	$list=$result->row;    


$car_model_name=$list['car_model_name'];
if($car_model_name=="")
   {
   echo "---------";
   }
    else
    {
    echo $car_model_name;
    }
?></td>
              </tr>
              <tr>
                <th class="">Car Number</th>
                <td class=""><?php
                      $carnumber=$driver['car_number'];
                      if($carnumber=="")
                      {
                      echo "---------";
                      }
                      else
                      {
                      echo $carnumber;
                      }
                   ?></td>
              </tr>
              <tr>
                <th class="">City Name</th>
                <td class=""><?php 

$city_id=$driver['city_id'];

$query="select * from city where city_id=$city_id";
	$result = $db->query($query);
	$list=$result->row;    


echo $list['city_name'];
?></td>
              </tr>
              <tr>
                <th class="">Register Date</th>
                <td class=""><?php
                      $registerdate=$driver['register_date'];
                      if($registerdate=="")
                      {
                      echo "---------";
                      }
                      else
                      {
                      echo $registerdate;
                      }
                   ?></td>
              </tr>
              <tr>
                <th class="">Current Location</th>
                <td class=""><?php
                      $currentlocation=$driver['current_location'];
                                                        if($currentlocation=="")
                                                           {
                                                           echo "---------";
                                                           }
                                                           else
                                                           {
                                                           echo $currentlocation;
                                                           }
                                                       ?></td>
              </tr>
              <tr>
                <th class="">Completed Rides</th>
                <td class=""><?php
                                                        $completedrides=$driver['completed_rides'];
                                                        if($completedrides=="")
                                                           {
                                                           echo "---------";
                                                           }
                                                           else
                                                           {
                                                           echo $completedrides;
                                                           }
                                                       ?></td>
              </tr>
              <tr>
                <th class="">Rejected Rides</th>
                <td class=""><?php
                                                        $rejectrides=$driver['reject_rides'];
                                                        if($rejectrides=="")
                                                           {
                                                           echo "---------";
                                                           }
                                                           else
                                                           {
                                                           echo $rejectrides;
                                                           }
                                                       ?></td>
              </tr>
              <tr>
                <th class="">Cancelled Rides</th>
                <td class=""><?php
                                                        $cancelledrides=$driver['cancelled_rides'];
                                                        if($cancelledrides=="")
                                                           {
                                                           echo "---------";
                                                           }
                                                           else
                                                           {
                                                           echo $cancelledrides;
                                                           }
                                                       ?></td>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
           <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
            <tfoot>
            	<tr>
                	<th>Licence</th>
                    <th>Registration Certificate (RC)</th>
                    <th>Insurance</th>
                </tr>
            	<tr>
					<?php $filenotexit="http://apporio.co.uk/apporiotaxi/uploads/driver/filenotexit.png"; ?>
                    <?php $fileexit="http://apporio.co.uk/apporiotaxi/uploads/driver/fileexit.png"; ?>
                	
                     <td>
                     	<a target="_blank" href="<?php if($driver['license'] != '' && isset($driver['license'])){echo '../'.$driver['license'];}else{ echo $filenotexit; }?>">
                        	<img src="<?php if($driver['license'] != '' && isset($driver['license'])){echo $fileexit;}else{ echo $filenotexit; }?>"  width="150px" height="150px">
                        </a>
                      </td>
                      
                     <td>
                     	<a target="_blank" href="<?php if($driver['rc'] != '' && isset($driver['rc'])){echo '../'.$driver['rc'];}else{ echo $filenotexit; }?>">
                        	<img src="<?php if($driver['rc'] != '' && isset($driver['rc'])){echo $fileexit;}else{ echo $filenotexit; }?>"  width="150px" height="150px">
                         </a>
                     </td>
                     <td>
                     	<a target="_blank" href="<?php if($driver['insurance'] != '' && isset($driver['insurance'])){echo '../'.$driver['insurance'];}else{ echo $filenotexit; }?>">
                        	<img src="<?php if($driver['insurance'] != '' && isset($driver['insurance'])){echo $fileexit;}else{ echo $filenotexit; }?>"  width="150px" height="150px">
                        </a>
                     </td>
                    
                </tr>
            </tfoot>
           </table>
        </div>
    </div>
    
    <!-- Modal content closed--> 
    
  </div>
</div>
<?php }?>

<script>
function getId(val) {

  $.ajax({
        type: "POST",
        url: "viewcar_model.php",
        data: "car_type_id="+val,
        success: 
        function(data){
       $('#car_model_id').html(data);
        }
    });  
}
 
</script> 
</section>
</body></html>