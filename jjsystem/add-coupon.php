<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');

if(isset($_POST['save'])) 
     {
$query2="INSERT INTO coupons (coupons_code,coupons_price,expiry_date,status) VALUES ('".$_POST['coupons_code']."','".$_POST['coupons_price']."','".$_POST['expiry_date']."',1)";
$db->query($query2);    
}
 
?>

  <!-- Page Content Start --> 
  <!-- ================== -->
  
<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add Coupon</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          
          <div class="panel-body">
            <div class="form" >
              <form class="cmxform form-horizontal tasi-form"  method="post" onSubmit="return validatelogin()">
                <div class="form-group ">
                  <label class="control-label col-lg-2">Coupon Code*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Coupon Code" name="coupons_code" id="" required>
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Coupon Price*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Coupon Price" name="coupons_price" id="" required>
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Expiry Date*</label>
                  <div class="col-lg-10">
                    <input type="date" class="form-control"placeholder="mm/dd/yyyy" id="datepicker-multiple" name="expiry_date"  required>
                  </div>
                </div>
                        
                
                
                
                
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Add Coupon" >
                  </div>
                </div>
              </form>
            </div>
            <!-- .form --> 
            
          </div>
          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
      
    </div>
    <!-- End row --> 
    
  </div>
  
  <!-- Page Content Ends --> 
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->

</body>
</html>
