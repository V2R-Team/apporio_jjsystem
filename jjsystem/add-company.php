<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');

if(isset($_POST['save'])) 
     {
         $company_role = "";
         $query="select * from company WHERE company_email='".$_POST['company_email']."'";
         $result = $db->query($query);
         $list=$result->row;
         if(count($list) == 0)
         {
             $query2="INSERT INTO company (company_name,company_email,company_phone,company_address,company_contact_person,company_password,company_role) VALUES ('".$_POST['company_name']."','".$_POST['company_email']."','".$_POST['company_phone']."','".$_POST['company_address']."','".$_POST['company_contact_person']."','".$_POST['company_password']."','$company_role')";
             $db->query($query2);
         }else{
             $errorMsg = "Email already in Registerd!";
         }
	}
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB21baMnSAvRrcZ3IKymum3_vJFN4hm7xU&v=3.exp&libraries=places"></script>

<script>
    function initialize() {

        var input = document.getElementById('company_address');

        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>

 


  <!-- Page Content Start --> 
  <!-- ================== -->
  
<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add Company Details</h3>
        <?php if(isset($errorMsg)){ ?>
        <h3 style="float:right; color:red;">Email already Registerd!</h3>
        <?php } ?>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          
          <div class="panel-body">
            <div class="form">
              <form class="cmxform form-horizontal tasi-form"  method="post">

                  <div class="form-group ">
                      <label class="control-label col-lg-2">Name</label>
                      <div class="col-lg-10">
                          <input type="text" class="form-control"  placeholder="Company Name" name="company_name" id="company_name" required>
                      </div>
                  </div>

                  <div class="form-group ">
                      <label class="control-label col-lg-2">Email</label>
                      <div class="col-lg-10">
                          <input type="email" class="form-control"  placeholder="Company Email" name="company_email" id="company_email" required>
                      </div>
                  </div>

                  <div class="form-group ">
                      <label class="control-label col-lg-2">Phone</label>
                      <div class="col-lg-10">
                          <input type="text" class="form-control"  placeholder="Company Phone" name="company_phone" id="company_phone" required>
                      </div>
                  </div>

                  <div class="form-group ">
                      <label class="control-label col-lg-2">Contact Person</label>
                      <div class="col-lg-10">
                          <input type="text" class="form-control"  placeholder="Company Contact Person" name="company_contact_person" id="company_contact_person" required>
                      </div>
                  </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Address</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control"  placeholder="Company Address" name="company_address" id="company_address" required>
                  </div>
                </div>

                  <div class="form-group ">
                      <label class="control-label col-lg-2">Default Password</label>
                      <div class="col-lg-10">
                          <input type="password" class="form-control"  placeholder="Default Password" name="company_password" id="company_password" required>
                      </div>
                  </div>
                
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
                  </div>
                </div>
              </form>
            </div>
            <!-- .form --> 
            
          </div>
          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
      
    </div>
    <!-- End row --> 
    
  </div>
  
  <!-- Page Content Ends --> 
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->

</body>
</html>
