<?php
include('common.php');
include_once '../apporioconfig/start_up.php';

?>

<div class="wraper container-fluid">
  <div class="page-title">
    <h3 class="title">Driver Maps</h3>
          <!-- <a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i style="color:#39c9cb;" class="fa fa-bars  fa-2x" ></i></a> -->
    <nav id="sidebar-wrapper" class="">
      <div id="driver_details">
        <ul class="sidebar-nav ">
            <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i style="color:#ffffff;" class="fa fa-times"></i></a>
            <li style="clear:both;"></li>

        </ul>
      </div>
    </nav>

  </div>
  <div class="row">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCOfKh2baXWXfu3rIQDnTnIWxviWpjSyNI"></script>
    <div id="map" style="width:100%;height:550px;  "></div>



    <script type="text/javascript">

var locations = [
<?php
  $query="select * from driver where online_offline=1 and busy != 1 ";
 $result = $db->query($query);
 $list=$result->rows;
 $tot = count($list);
 $jsonArr = array();
 foreach($list as $con) {
?>
['<div style=" text-transform: capitalize; cursor:pointer;" class="map_details_click" onclick="showmapdetails(<?php echo $con["driver_id"] ?>)" ><?php echo $con['driver_name'] ?></div>', <?php echo $con['current_lat'] ?>, <?php echo $con['current_long'] ?>,'<?php echo ICON_URL;?>'],
<?php }?>

<?php
  $query1="select * from driver where busy = 1 ";
 $result1 = $db->query($query1);
 $list1=$result1->rows;
 $tot1 = count($list1);
 $jsonArr1 = array();
 foreach($list1 as $con1) {

    $query2="select * from ride_table WHERE driver_id = '".$con1['driver_id']."' ORDER BY ride_id DESC LIMIT 0,1  ";
    $result2 = $db->query($query2);
    $list2 = $result2->rows;
    foreach($list2 as $con2);

    $query3="select * from bill_calculate LEFT JOIN driver ON bill_calculate.driver_id = driver.driver_id WHERE bill_calculate.driver_id = '".$con2['driver_id']."' AND bill_calculate.ride_id = '".$con2['ride_id']."'  ORDER BY latlong_id DESC LIMIT 0,1  ";
    $result3 = $db->query($query3);
    $list3 = $result3->rows;
    foreach($list3 as $con3);

?>
['<div style=" text-transform: capitalize; cursor:pointer;" class="map_details_click" onclick="showmapdetails(<?php echo $con3["driver_id"] ?>)" ><?php echo $con3['driver_name'] ?></div>', <?php echo $con3['driver_lat'] ?>, <?php echo $con3['driver_long'] ?>,'<?php echo ICON1_URL;?>'],
<?php }?>

];

//var locations = <?php echo $_json ?>;
//var locations = <?php echo $_json1 ?>;

var map = new google.maps.Map(document.getElementById('map'),{
zoom: 12,
center: new google.maps.LatLng(28.4120558, 77.0433644),
mapTypeId: google.maps.MapTypeId.ROADMAP
});
var infowindow = new google.maps.InfoWindow();

var marker, i;

for (i = 0; i < locations.length; i++) {

marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
                icon: locations[i][3]
            });
	google.maps.event.addListener(marker, 'click', (function(marker, i) {
		return function() {
			infowindow.setContent(locations[i][0]);
			infowindow.open(map, marker);
		}
	})(marker, i));
}

</script>


<script>

  function showmapdetails(driver_id) {
    $('#sidebar-wrapper').addClass("active");
    $.ajax({
     url: "ajax.php?action=driver_details1&driver_id="+driver_id,
     success: function(res) {
      $('#driver_details').html(res);
     }
    });
  }

  function showmapdetails1(driver_id) {
    $('#sidebar-wrapper').addClass("active");
    $.ajax({
     url: "ajax.php?action=driver_details1_ride&driver_id="+driver_id,
     success: function(res) {
      $('#driver_details').html(res);
     }
    });
  }


</script>
  </div>
</div>
</section>
<!-- Main Content Ends -->

</body></html>