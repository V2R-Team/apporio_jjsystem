<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');
$query="select * from car_type";
	$result = $db->query($query);
	$list=$result->rows;       
        
if(isset($_POST['save'])) {
		 
		    $query1="INSERT INTO car_model (car_model_name,car_type_id,status) VALUES('".$_POST['car_model_name']."','".$_POST['car_type_id']."',1)";
     		$db->query($query1);
}
?>



  <!-- Page Content Start --> 
  <!-- ================== -->
  
<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add Car Model</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          
          <div class="panel-body">
            <div class="form" >
              <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatelogin()">

                <div class="form-group ">
                  <label class="control-label col-lg-2">Choose Car Type*</label>
                  <div class="col-lg-10">
                     <select class="form-control" name="car_type_id" id="" required>
                        <option>--Please Select Car Type--</option>
                          <?php foreach($list as $cartype){ ?>
                            
                           <option id="<?php echo $cartype['car_type_id'];?>"  value="<?php echo $cartype['car_type_id'];?>"><?php echo $cartype['car_type_name']; ?></option>


                <?php } ?>
                    </select>
                    
                    
                 
                    
                  </div>
                </div>            

                <div class="form-group ">
                  <label class="control-label col-lg-2">Car Model Name*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Car Model Name" name="car_model_name" id="" required>
                  </div>
                </div>
                
                
                
                
                
                
                
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                   
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
                  </div>
                </div>
              </form>
            </div>
            <!-- .form --> 
            
          </div>
          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
      
    </div>
    <!-- End row --> 
    
  </div>
  
  <!-- Page Content Ends --> 
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->

</body>
</html>
