<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');
$query="select * from city";
	$result = $db->query($query);
	$list=$result->rows; 
	
$query="select * from car_type";
	$result = $db->query($query);
	$list1=$result->rows;       

   if(isset($_POST['save'])) {
     $query1="INSERT INTO rate_card (city_id,car_type_id,base_miles,base_minutes,base_price_miles,base_price_minute,price_per_mile,price_per_minute,base_waiting_minutes,base_price_minute_waiting,price_per_minute_waiting,extra_charges) VALUES('".$_POST['city_id']."','".$_POST['car_type_id']."','".$_POST['base_miles']."','".$_POST['base_minutes']."','".$_POST['base_price_miles']."','".$_POST['base_price_minute']."','".$_POST['price_per_mile']."','".$_POST['price_per_minute']."','".$_POST['base_waiting_minutes']."','".$_POST['base_price_minute_waiting']."','".$_POST['price_per_minute_waiting']."','".$_POST['extra_charges']."')";
     		$db->query($query1);
			
	}

?>
<script type="text/javascript">
  
  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

</script>

  <!-- Page Content Start --> 
  <!-- ================== -->
  
<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add Rate Card</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          
          <div class="panel-body">
            <div class="form" >
              <form class="cmxform form-horizontal tasi-form"  method="post">

                <div class="form-group ">
                  <label class="control-label col-lg-2">Choose City*</label>
                  <div class="col-lg-10">
                    <select class="form-control" name="city_id" id="" required>
                        <option>--Please Select City Name--</option>
                          <?php foreach($list as $cityname){ ?>
                           <option id="<?php echo $cityname['city_id'];?>"  value="<?php echo $cityname['city_id'];?>"><?php echo $cityname['city_name']; ?></option>


                <?php } ?>
                    </select>
                  </div>
                </div> 
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Choose Car Type Name*</label>
                  <div class="col-lg-10">
                    <select class="form-control" name="car_type_id" id="" required>
                        <option>--Please Select Car Type--</option>
                          <?php foreach($list1 as $cartype){ ?>
                            
                           <option id="<?php echo $cartype['car_type_id'];?>"  value="<?php echo $cartype['car_type_id'];?>"><?php echo $cartype['car_type_name']; ?></option>


                <?php } ?>
                    </select>
                  </div>
                </div>            

                <div class="form-group ">
                  <label class="control-label col-lg-2">Base Miles *</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Base Miles" name="base_miles" onkeypress="return isNumber(event)" id="" required>
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Base Minutes *</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Base Minutes" name="base_minutes" onkeypress="return isNumber(event)" id="" required>
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Base Price Miles *</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Base Price Miles" name="base_price_miles" onkeypress="return isNumber(event)" id="" required>
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Base Price Minute *</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Base Price Minute" name="base_price_minute" onkeypress="return isNumber(event)" id="" required>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Price Per Mile  *</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Price Per Mile" name="price_per_mile" onkeypress="return isNumber(event)" id="" required>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Price Per Minute *</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Price Per Minute" name="price_per_minute" onkeypress="return isNumber(event)" id="" required>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Base Waiting Minute *</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Base Waiting Minute" name="base_waiting_minutes" onkeypress="return isNumber(event)" id="" required>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Base Price Minute Waiting *</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Base Price Minute Waiting" onkeypress="return isNumber(event)" name="base_price_minute_waiting" id="" required>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Price Per Minute Waiting *</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Price Per Minute Waiting" onkeypress="return isNumber(event)" name="price_per_minute_waiting" id="" required>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Extra Charge *</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Extra Charge" onkeypress="return isNumber(event)" name="extra_charges" id="" required>
                  </div>
                </div>
                
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Add Rate Card" >
                  </div>
                </div>
              </form>
            </div>
            <!-- .form --> 
            
          </div>
          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
      
    </div>
    <!-- End row --> 
    
  </div>
  
  <!-- Page Content Ends --> 
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->

</body>
</html>
