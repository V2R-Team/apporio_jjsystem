<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');

if(isset($_POST['save'])) 
     {

	$city_name = $_POST['city_name'];
	$city = explode(",",$city_name);
	$query2="INSERT INTO city (city_name,status) VALUES ('".$city[0]."',1)";
	$db->query($query2); 
	}

?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCaak-nVeDAR2Rc2I28BcgRaQKv12kOWOo&v=3.exp&libraries=places"></script>
<script>
function initialize() {

var input = document.getElementById('city_name');
var options = {
  types: ['(cities)'],
};

var autocomplete = new google.maps.places.Autocomplete(input,options);
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
 


  <!-- Page Content Start --> 
  <!-- ================== -->
  
<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add City Name</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          
          <div class="panel-body">
            <div class="form">
              <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatelogin()">
                <div class="form-group ">
                  <label class="control-label col-lg-2">Add City*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control"  placeholder="Add City Name" name="city_name" id="city_name" required>
                  </div>
                </div>
                
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
                  </div>
                </div>
              </form>
            </div>
            <!-- .form --> 
            
          </div>
          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
      
    </div>
    <!-- End row --> 
    
  </div>
  
  <!-- Page Content Ends --> 
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->

</body>
</html>
