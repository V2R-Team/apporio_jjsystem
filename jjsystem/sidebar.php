<?php 
include_once '../apporioconfig/connection.php'; 

  $admin_url =  ADMIN_URL;
  $request = $_SERVER[REQUEST_URI];
 ?>
<aside class="left-panel"> 
  
  <!-- brand -->
  <div class="logo"> <a href="home.php?pages=dashboard" class="logo-expanded"> <img src="img/taxilogo.png" alt="logo"> <span class="nav-label">JJ System</span> </a> </div>
  <!-- / brand --> 
  
  <!-- Navbar Start -->
  <nav class="navigation">
    <ul class="list-unstyled">
      <!-- Dashboard Start -->
      <?php
        $li_open = $arr_open = $ul_open = "";
        if(empty($_REQUEST['pages'])) {
            $li_open    = "active open";
            $arr_open   = "open";
            $ul_open    = "style='display: block'";
        }
        ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=dashboard"><i class="glyphicon glyphicon-home" aria-hidden="true"></i> <span class="nav-label">Dashboard</span><span class="selected"></span></a></li>
<?php 
   $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
   if($actual_link == "$admin_url/drivers.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/add_booking.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/dashboard.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/account-setting.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/profile.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/change-password.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/transactions.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/add-car.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/view-car.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/add-car-model.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/view-car-model.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/add-rate-card.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/view-rate-card.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/add-company.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/ride-now.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/ride-later.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/ride-completed.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/view-company.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/add-coupon.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/view-coupons.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/about.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/terms.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/support.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/driver-map.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/accounts.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/add_role.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/view_role.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/add_subadmin.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/view_subadmin.php"){
    header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/add_company.php")
   {
       header("Location: home.php?pages=dashboard");
   }
   if($actual_link == "$admin_url/view_company.php")
   {
    header("Location: home.php?pages=dashboard");
   }
    if($actual_link == "$admin_url/add_city.php")
    {
        header("Location: home.php?pages=dashboard");
    }
    if($actual_link == "$admin_url/view_city.php")
    {
        header("Location: home.php?pages=dashboard");
    }


?>

 
      <?php if($_SESSION['ADMIN']['ROLE'] == ""){ ?>

        <!-- Add Company Start -->
        <?php
        $li_open = $arr_open = $ul_open = "";
        if(@$_REQUEST['pages'] == "add-company" || $_REQUEST['pages'] == "view-company") {
            $li_open    = "active open";
            $arr_open   = "open";
            $ul_open    = "style='display: block'";
        }
        ?>
        <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-building"></i> <span class="nav-label">Company</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?> "></span></a>
            <ul class="list-unstyled">
                <li><a href="home.php?pages=add-company">Add Company</a></li>
                <li><a href="home.php?pages=view-company">View Company</a></li>
            </ul>
        </li>
      
      <!-- Driver Start -->
      <?php
                            $li_open = $arr_open = $ul_open = "";
                            if(@$_REQUEST['pages'] == "view-driver") {
                                $li_open    = "active open";
                                $arr_open   = "open";
                                $ul_open    = "style='display: block'";
                            }
                            ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=view-driver"><i class="fa fa-user" aria-hidden="true"></i> <span class="nav-label" >Drivers</span><span class="selected"></span></a></li>

        <!-- Add City Start -->
        <?php
        $li_open = $arr_open = $ul_open = "";
        if(@$_REQUEST['pages'] == "add-city" || $_REQUEST['pages'] == "view-city") {
            $li_open    = "active open";
            $arr_open   = "open";
            $ul_open    = "style='display: block'";
        }
        ?>
        <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-road"></i> <span class="nav-label">City</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?> "></span></a>
            <ul class="list-unstyled">
                <li><a href="home.php?pages=add-city">Add City</a></li>
                <li><a href="home.php?pages=view-city">View City</a></li>
            </ul>
        </li>

        <!-- Map Start -->
      <?php
                            $li_open = $arr_open = $ul_open = "";
                            if(@$_REQUEST['pages'] == "driver-map") {
                                $li_open    = "active open";
                                $arr_open   = "open";
                                $ul_open    = "style='display: block'";
                            }
                            ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=driver-map"><i class="fa fa-map-marker" aria-hidden="true"></i> <span class="nav-label" >Driver Map</span><span class="selected"></span></a></li>
      
      
       <!-- Transactions Start -->
      <?php
                            $li_open = $arr_open = $ul_open = "";
                            if(@$_REQUEST['pages'] == "view-transactions") {
                                $li_open    = "active open";
                                $arr_open   = "open";
                                $ul_open    = "style='display: block'";
                            }
                            ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=view-transactions"><i class="fa fa-user" aria-hidden="true"></i> <span class="nav-label" >Transactions</span><span class="selected"></span></a></li>


      <!-- Add car Start -->
      <?php
                            $li_open = $arr_open = $ul_open = "";
                            if(@$_REQUEST['pages'] == "add-car-type" || $_REQUEST['pages'] == "view-car-type") {
                                $li_open    = "active open";
                                $arr_open   = "open";
                                $ul_open    = "style='display: block'";
                            }
                            ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-car" aria-hidden="true"></i> <span class="nav-label" >Car Type</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?>"></span></a>
        <ul class="list-unstyled">
          <li><a href="home.php?pages=add-car-type">Add Car Type</a></li>
          <li><a href="home.php?pages=view-car-type">View Car Type</a></li>
        </ul>
      </li>
      
      <!-- Add car model Start -->
      <?php
                            $li_open = $arr_open = $ul_open = "";
                            if(@$_REQUEST['pages'] == "add-car-model" || $_REQUEST['pages'] == "view-car-model") {
                                $li_open    = "active open";
                                $arr_open   = "open";
                                $ul_open    = "style='display: block'";
                            }
                         ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-taxi" aria-hidden="true"></i> <span class="nav-label">Car Model</span><span class="selected"></span><span class="arrow <?php echo $arr_open ?>"></span></a>
        <ul class="list-unstyled">
          <li><a href="home.php?pages=add-car-model">Add Car Model</a></li>
          <li><a href="home.php?pages=view-car-model">View Car Model</a></li>
        </ul>
      </li>
      
      <!-- Add Coupons Start -->
      <?php
                            $li_open = $arr_open = $ul_open = "";
                            if(@$_REQUEST['pages'] == "add-coupons" || $_REQUEST['pages'] == "view-coupons") {
                                $li_open    = "active open";
                                $arr_open   = "open";
                                $ul_open    = "style='display: block'";
                            }
                            ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-tags"></i> <span class="nav-label">Coupons</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?> "></span></a>
        <ul class="list-unstyled">
          <li><a href="home.php?pages=add-coupons">Add Coupon</a></li>
          <li><a href="home.php?pages=view-coupons">View Coupons</a></li>
        </ul>
      </li>
      
      <!-- Add Rate card Start -->
      <?php
                            $li_open = $arr_open = $ul_open = "";
                            if(@$_REQUEST['pages'] == "add-rate-card" || $_REQUEST['pages'] == "view-rate-card") {
                                $li_open    = "active open";
                                $arr_open   = "open";
                                $ul_open    = "style='display: block'";
                            }
                            ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-star"></i> <span class="nav-label">Rate Card</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?> "></span></a>
        <ul class="list-unstyled">
          <li><a href="home.php?pages=add-rate-card">Add Rate Card</a></li>
          <li><a href="home.php?pages=view-rate-card">View Rate Card</a></li>
        </ul>
      </li>
      
      
      
      
      <!-- Rides Start -->
      <?php
                            $li_open = $arr_open = $ul_open = "";
                            if(@$_REQUEST['pages'] == "ride-now" || $_REQUEST['pages'] == "ride-later") {
                                $li_open    = "active open";
                                $arr_open   = "open";
                                $ul_open    = "style='display: block'";
                            }
                         ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-taxi" aria-hidden="true"></i> <span class="nav-label">Booking</span><span class="selected"></span><span class="arrow <?php echo $arr_open ?>"></span></a>
        <ul class="list-unstyled">
          <li><a href="home.php?pages=ride-now">Book Now</a></li>
          <li><a href="home.php?pages=ride-later">Book Later</a></li>
        </ul>
      </li>
      
      <!-- user Start -->
      <?php
                            $li_open = $arr_open = $ul_open = "";
                            if(@$_REQUEST['pages'] == "ride-completed") {
                                $li_open    = "active open";
                                $arr_open   = "open";
                                $ul_open    = "style='display: block'";
                            }
                            ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=ride-completed"><i class="fa fa-taxi" aria-hidden="true"></i> <span class="nav-label" >Ride Completed</span><span class="selected"></span></a></li>
      
      
      <!--Pages Starts-->
       <?php
                            $li_open = $arr_open = $ul_open = "";
                            if(@$_REQUEST['pages'] == "page-about-us" || $_REQUEST['pages'] == "page-terms-condition" || $_REQUEST['pages'] == "page-call-support") {
                                $li_open    = "active open";
                                $arr_open   = "open";
                                $ul_open    = "style='display: block'";
                            }
                         ?>
      
       
      <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-external-link" aria-hidden="true"></i> <span class="nav-label">Pages</span><span class="selected"></span><span class="arrow <?php echo $arr_open ?>"></span></a>
        <ul class="list-unstyled">
          <li><a href="home.php?pages=page-about-us">About Us</a></li>
          <li><a href="home.php?pages=page-terms-condition">Terms & Condition</a></li>
          <li><a href="home.php?pages=page-call-support">Call Support</a></li>
        </ul>
      </li>

      <!-- Add Role -->
      <?php
          $li_open = $arr_open = $ul_open = "";
          if(@$_REQUEST['pages'] == "add-role" || $_REQUEST['pages'] == "view-role") {
              $li_open    = "active open";
              $arr_open   = "open";
              $ul_open    = "style='display: block'";
          }
       ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-users" aria-hidden="true"></i> <span class="nav-label">Role</span><span class="selected"></span><span class="arrow <?php echo $arr_open ?>"></span></a>
        <ul class="list-unstyled">
          <li><a href="home.php?pages=add-role">Add Role</a></li>
          <li><a href="home.php?pages=view-role">View Role</a></li>
        </ul>
      </li>

      <!-- Add Sub Admin -->
      <?php
          $li_open = $arr_open = $ul_open = "";
          if(@$_REQUEST['pages'] == "add-subadmin" || $_REQUEST['pages'] == "view-subadmin") {
              $li_open    = "active open";
              $arr_open   = "open";
              $ul_open    = "style='display: block'";
          }
       ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-users" aria-hidden="true"></i> <span class="nav-label">Sub Admin</span><span class="selected"></span><span class="arrow <?php echo $arr_open ?>"></span></a>
        <ul class="list-unstyled">
          <li><a href="home.php?pages=add-subadmin">Add Sub Admin</a></li>
          <li><a href="home.php?pages=view-subadmin">View Sub Admin</a></li>
        </ul>
      </li>
      
       
    <!-- <li class="has-submenu"><a href="#"><i class="fa fa-bell"></i> <span class="nav-label">Push Notification</span></a></li>-->
    </ul>
    <ul class="list-unstyled">
      <li><a href="home.php?pages=accounts"><i class="fa fa-user" aria-hidden="true"></i>Accounts</a></li>
    </ul>

    <?php }else{  

        $query="select * from role WHERE role_id = '".$_SESSION['ADMIN']['ROLE']."' ";
        $result = $db->query($query);
        $list=$result->rows;
        foreach($list as $lists);
        $data =  json_decode(html_entity_decode($lists['role_permission']), true);
       
    ?>

        <?php if($data[add_company] == 1 OR $data[view_company] == 1){  ?>
            <!-- Add city model Start -->
            <?php
            $li_open = $arr_open = $ul_open = "";
            if(@$_REQUEST['pages'] == "add-company" || $_REQUEST['pages'] == "view-company") {
                $li_open    = "active open";
                $arr_open   = "open";
                $ul_open    = "style='display: block'";
            }
            ?>
            <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-road"></i> <span class="nav-label">Company</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?> "></span></a>
                <ul class="list-unstyled">

                    <?php if($data[add_company] == 1){  ?>
                        <li><a href="home.php?pages=add-company">Add Company</a></li>
                    <?php } if($data[view_company] == 1){  ?>
                        <li><a href="home.php?pages=view-company">View Company</a></li>
                    <?php } ?>
                </ul>
            </li>
        <?php }  ?>

    <?php 
          if($data[driver_view] == 1){  ?>
      <!-- Driver Start -->
       <?php
                            $li_open = $arr_open = $ul_open = "";
                            if(@$_REQUEST['pages'] == "view-driver") {
                                $li_open    = "active open";
                                $arr_open   = "open";
                                $ul_open    = "style='display: block'";
                            }
                            ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=view-driver"><i class="fa fa-user" aria-hidden="true"></i> <span class="nav-label" >Drivers</span><span class="selected"></span></a></li>
      <!-- Map Start -->
      <?php
        $li_open = $arr_open = $ul_open = "";
        if(@$_REQUEST['pages'] == "driver-map") {
            $li_open    = "active open";
            $arr_open   = "open";
            $ul_open    = "style='display: block'";
        }
        ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=driver-map"><i class="fa fa-map-marker" aria-hidden="true"></i> <span class="nav-label" >Driver Map</span><span class="selected"></span></a></li>
      <?php }  ?>


      <?php if($data[transaction_view] == 1){  ?>
       <!-- Transactions Start -->
      <?php
        $li_open = $arr_open = $ul_open = "";
        if(@$_REQUEST['pages'] == "view-transactions") {
            $li_open    = "active open";
            $arr_open   = "open";
            $ul_open    = "style='display: block'";
        }
        ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=view-transactions"><i class="fa fa-user" aria-hidden="true"></i> <span class="nav-label" >Transactions</span><span class="selected"></span></a></li>
      <?php }  ?>
      
      <?php if($data[city_add] == 1 OR $data[city_view] == 1){  ?>
     <!-- Add city model Start -->
      <?php
        $li_open = $arr_open = $ul_open = "";
        if(@$_REQUEST['pages'] == "add-city" || $_REQUEST['pages'] == "view-city") {
            $li_open    = "active open";
            $arr_open   = "open";
            $ul_open    = "style='display: block'";
        }
        ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-road"></i> <span class="nav-label">City</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?> "></span></a>
        <ul class="list-unstyled">

        <?php if($data[city_add] == 1){  ?>
          <li><a href="home.php?pages=add-city">Add City</a></li>
        <?php } if($data[city_view] == 1){  ?>
          <li><a href="home.php?pages=view-city">View City</a></li>
        <?php } ?>
        </ul>
      </li>
      <?php }  ?>

      <?php if($data[cartype_add] == 1 OR $data[cartype_view] == 1){  ?>
      <!-- Add car Start -->
      <?php
        $li_open = $arr_open = $ul_open = "";
        if(@$_REQUEST['pages'] == "add-car-type" || $_REQUEST['pages'] == "view-car-type") {
            $li_open    = "active open";
            $arr_open   = "open";
            $ul_open    = "style='display: block'";
        }
        ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-car" aria-hidden="true"></i> <span class="nav-label" >Car Type</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?>"></span></a>
        <ul class="list-unstyled">

        <?php if($data[cartype_add] == 1){  ?>
          <li><a href="home.php?pages=add-car-type">Add Car Type</a></li>
        <?php } if($data[cartype_view] == 1){  ?>
          <li><a href="home.php?pages=view-car-type">View Car Type</a></li>
        <?php } ?>
  
        </ul>
      </li>
      <?php }  ?>

      <?php if($data[carmodel_add] == 1 OR $data[carmodel_view] == 1){  ?>
      <!-- Add car model Start -->
      <?php
          $li_open = $arr_open = $ul_open = "";
          if(@$_REQUEST['pages'] == "add-car-model" || $_REQUEST['pages'] == "view-car-model") {
              $li_open    = "active open";
              $arr_open   = "open";
              $ul_open    = "style='display: block'";
          }
       ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-taxi" aria-hidden="true"></i> <span class="nav-label">Car Model</span><span class="selected"></span><span class="arrow <?php echo $arr_open ?>"></span></a>
        <ul class="list-unstyled">

        <?php if($data[carmodel_add] == 1){  ?>
          <li><a href="home.php?pages=add-car-model">Add Car Model</a></li>
        <?php } if($data[carmodel_view] == 1){  ?>
          <li><a href="home.php?pages=view-car-model">View Car Model</a></li>
        <?php } ?>

        </ul>
      </li>
      <?php }  ?>
      
      <?php if($data[coupon_add] == 1 OR $data[coupon_view] == 1){  ?>
      <!-- Add Coupons Start -->
      <?php
        $li_open = $arr_open = $ul_open = "";
        if(@$_REQUEST['pages'] == "add-coupons" || $_REQUEST['pages'] == "view-coupons") {
            $li_open    = "active open";
            $arr_open   = "open";
            $ul_open    = "style='display: block'";
        }
        ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-tags"></i> <span class="nav-label">Coupons</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?> "></span></a>
        <ul class="list-unstyled">

        <?php if($data[coupon_add] == 1){  ?>
          <li><a href="home.php?pages=add-coupons">Add Coupon</a></li>
        <?php } if($data[coupon_view] == 1){  ?>
          <li><a href="home.php?pages=view-coupons">View Coupons</a></li>
        <?php } ?>

        </ul>
      </li>
      <?php }  ?>
      
      <?php if($data[ratecard_add] == 1 OR $data[ratecard_view] == 1){  ?>
      <!-- Add Rate card Start -->
      <?php
        $li_open = $arr_open = $ul_open = "";
        if(@$_REQUEST['pages'] == "add-rate-card" || $_REQUEST['pages'] == "view-rate-card") {
            $li_open    = "active open";
            $arr_open   = "open";
            $ul_open    = "style='display: block'";
        }
        ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-star"></i> <span class="nav-label">Rate Card</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?> "></span></a>
        <ul class="list-unstyled">

        <?php if($data[ratecard_add] == 1){  ?>
          <li><a href="home.php?pages=add-rate-card">Add Rate Card</a></li>
        <?php } if($data[ratecard_view] == 1){  ?>
          <li><a href="home.php?pages=view-rate-card">View Rate Card</a></li>
        <?php } ?>

        </ul>
      </li>
      <?php }  ?>
      
      
      <?php if($data[view_now] == 1 OR $data[view_later] == 1){  ?>
      <!-- Rides Start -->
      <?php
          $li_open = $arr_open = $ul_open = "";
          if(@$_REQUEST['pages'] == "ride-now" || $_REQUEST['pages'] == "ride-later") {
              $li_open    = "active open";
              $arr_open   = "open";
              $ul_open    = "style='display: block'";
          }
       ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-taxi" aria-hidden="true"></i> <span class="nav-label">Booking</span><span class="selected"></span><span class="arrow <?php echo $arr_open ?>"></span></a>
        <ul class="list-unstyled">

        <?php if($data[view_now] == 1){  ?>
          <li><a href="home.php?pages=ride-now">Book Now</a></li>
        <?php } if($data[view_later] == 1){  ?>
          <li><a href="home.php?pages=ride-later">Book Later</a></li>
        <?php } ?>
      
        </ul>
      </li>
      <?php }  ?>

      <?php if($data[ride_view] == 1 ){  ?>
      <!-- user Start -->
      <?php
        $li_open = $arr_open = $ul_open = "";
        if(@$_REQUEST['pages'] == "ride-completed") {
            $li_open    = "active open";
            $arr_open   = "open";
            $ul_open    = "style='display: block'";
        }
        ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=ride-completed"><i class="fa fa-taxi" aria-hidden="true"></i> <span class="nav-label" >Ride Completed</span><span class="selected"></span></a></li>
      <?php }  ?>

      <?php if($data[about] == 1 OR $data[terms] == 1 OR $data[call] == 1 ){  ?>
      <!--Pages Starts-->
       <?php
          $li_open = $arr_open = $ul_open = "";
          if(@$_REQUEST['pages'] == "page-about-us" || $_REQUEST['pages'] == "page-terms-condition" || $_REQUEST['pages'] == "page-call-support") {
              $li_open    = "active open";
              $arr_open   = "open";
              $ul_open    = "style='display: block'";
          }
       ?>
       
      <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-external-link" aria-hidden="true"></i> <span class="nav-label">Pages</span><span class="selected"></span><span class="arrow <?php echo $arr_open ?>"></span></a>
        <ul class="list-unstyled">

        <?php if($data[about] == 1){  ?>
          <li><a href="home.php?pages=page-about-us">About Us</a></li>
        <?php } if($data[terms] == 1){  ?>
          <li><a href="home.php?pages=page-terms-condition">Terms & Condition</a></li>
        <?php } if($data[call] == 1){  ?>
          <li><a href="home.php?pages=page-call-support">Call Support</a></li>
        <?php } ?>

        </ul>
      </li>
      <?php }  ?>

      <?php if($data[role_add] == 1 OR $data[role_view] == 1  ){  ?>
      <!-- Add Role -->
      <?php
          $li_open = $arr_open = $ul_open = "";
          if(@$_REQUEST['pages'] == "add-role" || $_REQUEST['pages'] == "view-role") {
              $li_open    = "active open";
              $arr_open   = "open";
              $ul_open    = "style='display: block'";
          }
       ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-users" aria-hidden="true"></i> <span class="nav-label">Role</span><span class="selected"></span><span class="arrow <?php echo $arr_open ?>"></span></a>
        <ul class="list-unstyled">

        <?php if($data[role_add] == 1){  ?>
          <li><a href="home.php?pages=add-role">Add Role</a></li>
        <?php } if($data[role_view] == 1){  ?>
          <li><a href="home.php?pages=view-role">View Role</a></li>
        <?php } ?>
          
        </ul>
      </li>
      <?php }  ?>

      <?php if($data[subadmin_add] == 1 OR $data[subadmin_view] == 1 ){  ?>

      <!-- Add Sub Admin -->
      <?php
          $li_open = $arr_open = $ul_open = "";
          if(@$_REQUEST['pages'] == "add-subadmin" || $_REQUEST['pages'] == "view-subadmin") {
              $li_open    = "active open";
              $arr_open   = "open";
              $ul_open    = "style='display: block'";
          }
       ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-users" aria-hidden="true"></i> <span class="nav-label">Sub Admin</span><span class="selected"></span><span class="arrow <?php echo $arr_open ?>"></span></a>
        <ul class="list-unstyled">

        <?php if($data[subadmin_add] == 1){  ?>
          <li><a href="home.php?pages=add-subadmin">Add Sub Admin</a></li>
        <?php } if($data[subadmin_view] == 1){  ?>
          <li><a href="home.php?pages=view-subadmin">View Sub Admin</a></li>
        <?php } ?>

          
          
        </ul>
      </li>
      
       
    <!-- <li class="has-submenu"><a href="#"><i class="fa fa-bell"></i> <span class="nav-label">Push Notification</span></a></li>-->
    </ul>
    <ul class="list-unstyled">
    <?php }  ?>

      <?php if($data[account] == 1 ){  ?>
      <li><a href="home.php?pages=accounts"><i class="fa fa-user" aria-hidden="true"></i>Accounts</a></li>
      <?php }  ?>
    </ul>

    <?php  } ?>
  </nav>
</aside>
