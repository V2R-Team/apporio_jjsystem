<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');
if(isset($_POST['save'])) 
     {

$query2="INSERT INTO car_type (car_type_name,status) VALUES ('".$_POST['car_type_name']."',1)";
$db->query($query2); 

$car_type_id = $db->getLastId();



if(!empty($_FILES['car_type_image'])) 
  {
   $img_name = $_FILES['car_type_image']['name'];
   $filedir  = "../uploads/car/";
   if(!is_dir($filedir)) mkdir($filedir, 0755, true);
   $fileext = strtolower(substr($_FILES['car_type_image']['name'],-4));
   if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg") 
   {
    if($fileext=="jpeg") 
    {
     $fileext=".jpg";
    }
    $pfilename = "car_".$car_type_id.$fileext;
    $filepath1 = "uploads/car/".$pfilename;
    $filepath = $filedir.$pfilename;
   //if(file_exists("../".$user_image)) unlink("../".$user_image);
    copy($_FILES['car_type_image']['tmp_name'], $filepath);
      
    $upd_qry = "UPDATE car_type SET car_type_image ='$filepath1' where car_type_id ='$car_type_id'";
    $db->query($upd_qry);
   }
  }    
}

?>

<script type="text/javascript">
$(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
        function () {
           $('.image-preview').popover('show');
        }, 
         function () {
           $('.image-preview').popover('hide');
        }
    );    
});

$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
        content: "There's no image",
        placement:'bottom'
    });
    // Clear event
    $('.image-preview-clear').click(function(){
        $('.image-preview').attr("data-content","").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("Browse"); 
    }); 
    // Create the preview image
    $(".image-preview-input input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text("Change");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);            
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
        }        
        reader.readAsDataURL(file);
    });  
});
</script>

  <div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add Car Type</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          
          <div class="panel-body">
            <div class=" form" >
              <form class="cmxform form-horizontal tasi-form"  method="post" enctype="multipart/form-data" onSubmit="return validatelogin()">
                <div class="form-group ">
                  <label for="lastname" class="control-label col-lg-2">Car Type Name  *</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Car Type Name" name="car_type_name" id="car_type_name" required/>
                  </div>
                </div>
                <div class="form-group ">
                  <label for="username" class="control-label col-lg-2">Upload Image*</label>
                  <div class="col-lg-10">

                    <div class="input-group image-preview">
                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>
                    <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title">Browse</span>
                        <input type="file" accept="image/png, image/jpeg, image/gif" name="car_type_image" id="car_type_image" required/> <!-- rename it -->
                    </div>
                </span>
            </div>

                  </div>
                </div>
                
                
                
                
                
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
                  </div>
                </div>
              </form>
            </div>
            <!-- .form --> 
            
          </div>
          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
      
    </div>
    <!-- End row --> 
    
  </div>
  
  <!-- Page Content Ends --> 
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->

</body>
</html>
