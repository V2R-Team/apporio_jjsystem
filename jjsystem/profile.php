<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');

$query="select * from admin where admin_id=1";
	$result = $db->query($query);
	$list=$result->row;
	
	
	if(isset($_POST['personal'])) 
    {		
	  $query2="UPDATE admin SET admin_fname='".$_POST['admin_fname']."', admin_lname='".$_POST['admin_lname']."', admin_phone='".$_POST['admin_phone']."', admin_email='".$_POST['admin_email']."', admin_skype='".$_POST['admin_skype']."', admin_desc='".$_POST['admin_desc']."' WHERE admin_id=1";
	  $db->query($query2); 
	  $db->redirect("home.php?pages=profile");
	}
	
	if(isset($_POST['address'])) 
    {		
	  $query2="UPDATE admin SET admin_add='".$_POST['admin_add']."', admin_country='".$_POST['admin_country']."', admin_state='".$_POST['admin_state']."', admin_city='".$_POST['admin_city']."', admin_zip='".$_POST['admin_zip']."' WHERE admin_id=1";
	  $db->query($query2); 
	  $db->redirect("home.php?pages=profile");
	}
	
	if(isset($_POST['social'])) 
    {		
	  $query2="UPDATE admin SET admin_fb='".$_POST['admin_fb']."', admin_tw='".$_POST['admin_tw']."', admin_goo='".$_POST['admin_goo']."', admin_insta='".$_POST['admin_insta']."', admin_dribble='".$_POST['admin_dribble']."' WHERE admin_id=1";
	  $db->query($query2); 
	  $db->redirect("home.php?pages=profile");
	}
	
	

$dummyImg="http://apporio.co.uk/apporiotaxi/apporiotaxi/uploads/driver/driverprofile.png";

?>

<div class="wraper container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="bg-picture" style="background-image:url('../uploads/admin/cover.jpg')"> <span class="bg-picture-overlay"></span><!-- overlay --> 
        <!-- meta -->
        <div class="box-layout meta bottom">
          <div class="col-sm-6 clearfix"> <span class="img-wrapper pull-left m-r-15"> <img src="<?php if($list['admin_img'] != '' && isset($list['admin_img'])){echo '../'.$list['admin_img'];}else{ echo $dummyImg; }?>" style="width:64px;" class="br-radius"></span>
            <div class="media-body">
              <h3 class="text-white mb-2 m-t-10 ellipsis"><?php echo $list['admin_fname'];?></h3>
              <h5 class="text-white"><?php echo $list['admin_city'];?></h5>
            </div>
          </div>
        </div>
        <!--/ meta --> 
      </div>
    </div>
  </div>
  <div class="row m-t-30">
    <div class="col-sm-12">
      <div class="panel panel-default p-0">
        <div class="panel-body p-0">
          <ul class="nav nav-tabs profile-tabs">
            <li class="active"><a data-toggle="tab" href="#aboutme">About Me</a></li>
            <li class=""><a data-toggle="tab" href="#personal">Edit Personal info</a></li>
            <li class=""><a data-toggle="tab" href="#address">Edit Address</a></li>
            <li class=""><a data-toggle="tab" href="#social">Social Media</a></li>
          </ul>
          <div class="tab-content m-0">
            <div id="aboutme" class="tab-pane active">
              <div class="profile-desk">
                <h1><?php echo $list['admin_fname'];?>&nbsp;<?php echo $list['admin_lname'];?></h1>
                <span class="designation"></span>
                <p> <?php echo $list['admin_desc'];?> </p>
                <table class="table table-condensed">
                  <thead>
                    <tr>
                      <th colspan="3"><h3>Contact Information</h3></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><b>Phone</b></td>
                      <td>
                      <a href="tel:+<?php echo $list['admin_phone'];?>" class="ng-binding"> 
					  	<?php
					  		$admin_phone=$list['admin_phone'];
							if($admin_phone=="")
							{
								echo "------";
							}
							else
							{
								echo $admin_phone;
							}
					  	?> 
                      </a>
                    </td>
                    </tr>
                    <tr>
                      <td><b>Email</b></td>
                     <td>
                      <a href="mailto:<?php echo $list['admin_email'];?>" class="ng-binding">
                      	<?php
					  		$admin_email=$list['admin_email'];
							if($admin_email=="")
							{
								echo "------";
							}
							else
							{
								echo $admin_email;
							}
					  	?>  
                      </a>
                     </td>
                    </tr>
                    <tr>
                    	<td><b>Skype</b></td>
                        <td class="ng-binding">
						  <?php
					  		$admin_skype=$list['admin_skype'];
							if($admin_skype=="")
							{
								echo "------";
							}
							else
							{
								echo $admin_skype;
							}
					  	  ?>  
                        </td>
                    </tr>
                    <tr>
                      <td><b>Facebook</b></td>
                      <td>
                      	<a target="_blank" href="<?php echo $list['admin_fb'];?>" class="ng-binding">
                          <?php
					  		$admin_fb=$list['admin_fb'];
							if($admin_fb=="")
							{
								echo "------";
							}
							else
							{
								echo $admin_fb;
							}
					  	  ?>  
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td><b>Twitter</b></td>
                      <td>
                      	<a target="_blank" href="<?php echo $list['admin_tw'];?>" class="ng-binding"> 
						  <?php
					  		$admin_tw=$list['admin_tw'];
							if($admin_tw=="")
							{
								echo "------";
							}
							else
							{
								echo $admin_tw;
							}
					  	  ?>   
                        </a>
                      </td>
                    </tr>
                     <tr>
                      <td><b>Google +</b></td>
                      <td>
                      	<a target="_blank" href="<?php echo $list['admin_goo'];?>" class="ng-binding">
                          <?php
					  		$admin_goo=$list['admin_goo'];
							if($admin_goo=="")
							{
								echo "------";
							}
							else
							{
								echo $admin_goo;
							}
					  	  ?>   
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td><b>Instagram</b></td>
                      <td>
                      	<a target="_blank" href="<?php echo $list['admin_insta'];?>" class="ng-binding">
                          <?php
					  		$admin_insta=$list['admin_insta'];
							if($admin_insta=="")
							{
								echo "------";
							}
							else
							{
								echo $admin_insta;
							}
					  	  ?>   
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td><b>Dribble</b></td>
                      <td>
                      	<a target="_blank" href="<?php echo $list['admin_dribble'];?>" class="ng-binding">
                          <?php
					  		$admin_dribble=$list['admin_dribble'];
							if($admin_dribble=="")
							{
								echo "------";
							}
							else
							{
								echo $admin_dribble;
							}
					  	  ?>  
                        </a>
                      </td>
                    </tr>
                    
                  </tbody>
                </table>
              </div>
              <!-- end profile-desk --> 
            </div>
            
         
           
            
            <!-- settings -->
            <div id="personal" class="tab-pane">
              <div class="user-profile-content">
                <form role="form" method="post" onSubmit="return validatelogin()">
                  <div class="form-group">
                    <label>First Name</label>
                       <input type="text" class="form-control" placeholder="First Name" value="<?php echo $list['admin_fname'];?>" name="admin_fname" id="admin_fname" required>

                  </div>
                  <div class="form-group">
                    <label>Last Name</label>
                       <input type="text" class="form-control" placeholder="Last Name" value="<?php echo $list['admin_lname'];?>" name="admin_lname" id="admin_lname">

                  </div>
                  <div class="form-group">
                    <label>Phone Number</label>
                       <input type="text" class="form-control" placeholder="Phone Number" value="<?php echo $list['admin_phone'];?>" name="admin_phone" id="admin_phone">

                  </div>
                  <div class="form-group">
                    <label>Email Address</label>
                     <input type="email" class="form-control" placeholder="Email Address" value="<?php echo $list['admin_email'];?>" name="admin_email" id="admin_email" required>
                  </div>
                   <div class="form-group">
                    <label>Skype Id</label>
                     <input type="text" class="form-control" placeholder="Skype Id" value="<?php echo $list['admin_skype'];?>" name="admin_skype" id="admin_skype" required>
                  </div>
                 
                  
                 
                  <div class="form-group">
                    <label>Description</label>
                    <textarea style="height: 125px;" id="admin_desc" name="admin_desc" class="form-control"><?php echo $list['admin_desc'] ?></textarea>
                  </div>
                  <button class="btn btn-primary" id="personal" name="personal" type="submit">Save</button>
                </form>
              </div>
            </div>
            
            
            <div id="address" class="tab-pane">
              <div class="user-profile-content">
                <form role="form" method="post" onSubmit="return validatelogin()">
                  
                  
                  <div class="form-group">
                    <label>Address</label>
                     <input type="text" class="form-control" placeholder="Address" value="<?php echo $list['admin_add'];?>" name="admin_add" id="admin_add">
                  </div>
                  
                  <div class="form-group">
                    <label>Country</label>
                     <input type="text" class="form-control" placeholder="Country" value="<?php echo $list['admin_country'];?>" name="admin_country" id="admin_country">
                  </div>
                  
                  <div class="form-group">
                    <label>State</label>
                     <input type="text" class="form-control" placeholder="State" value="<?php echo $list['admin_state'];?>" name="admin_state" id="admin_state">
                  </div>
                  
                  <div class="form-group">
                    <label>City</label>
                     <input type="text" class="form-control" placeholder="City" value="<?php echo $list['admin_city'];?>" name="admin_city" id="admin_city">
                  </div>
                  
                  <div class="form-group">
                    <label>Zip Code</label>
                     <input type="number" class="form-control" placeholder="Zip Code" value="<?php echo $list['admin_zip'];?>" name="admin_zip" id="admin_zip">
                  </div>
                 
                  
                  <button class="btn btn-primary" name="address" id="address" type="submit">Save</button>
                </form>
              </div>
            </div>
            
            <div id="social" class="tab-pane">
              <div class="user-profile-content">
                <form role="form"  method="post" onSubmit="return validatelogin()">
                  
                  
                  <div class="form-group">
                    <label>Facebook</label>
                     <input type="text" class="form-control" placeholder="Facebook" value="<?php echo $list['admin_fb'];?>" name="admin_fb" id="admin_fb">
                  </div>
                  
                  <div class="form-group">
                    <label>Twitter</label>
                     <input type="text" class="form-control" placeholder="Twitter" value="<?php echo $list['admin_tw'];?>" name="admin_tw" id="admin_tw">
                  </div>
                  
                  <div class="form-group">
                    <label>Google Plus</label>
                     <input type="text" class="form-control" placeholder="State" value="<?php echo $list['admin_goo'];?>" name="admin_goo" id="admin_goo">
                  </div>
                  
                  <div class="form-group">
                    <label>Instagram</label>
                     <input type="text" class="form-control" placeholder="Instagram" value="<?php echo $list['admin_insta'];?>" name="admin_insta" id="admin_insta">
                  </div>
                  
                  <div class="form-group">
                    <label>Dribble</label>
                     <input type="text" class="form-control" placeholder="Dribble" value="<?php echo $list['admin_dribble'];?>" name="admin_dribble" id="admin_dribble">
                  </div>
                 
                  
                  <button class="btn btn-primary" name="social" id="social" type="submit">Save</button>
                </form>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<!-- Main Content Ends -->

</body></html>