<?php
	include "../apporioconfig/start_up.php";

if($_SESSION['ADMIN']['ROLE'] == ""){

	switch($_REQUEST['pages']) {

		case "dashboard" : include "dashboard.php"; break;

		case "account-setting" : include "account-setting.php"; break;

		case "profile" : include "profile.php"; break;

		case "change-password" : include "change-password.php"; break;

		case "view-state" : include "view_state.php"; break;
		
		case "booking_now" : include "add_booking.php"; break;

		case "booking-form" : include "booking-form.php"; break;

		case "view-driver" : include "drivers.php"; break;

		case "view-transactions" : include "transactions.php"; break;

		case "add-car-type" : include "add-car.php"; break;

		case "view-car-type" : include "view-car.php"; break;
		
		case "add-car-model" : include "add-car-model.php"; break;

		case "view-car-model" : include "view-car-model.php"; break;

		case "add-rate-card" : include "add-rate-card.php"; break;

		case "view-rate-card" : include "view-rate-card.php"; break;

		case "view-user" : include "users.php"; break;

		case "add-company" : include "add-company.php"; break;
		
		case "ride-now" : include "ride-now.php"; break;
		
		case "ride-later" : include "ride-later.php"; break;

		case "ride-completed" : include "ride-completed.php"; break;
		
		case "push-messages" : include "push_messages.php"; break;

		case "view-company" : include "view-company.php"; break;

		case "add-coupons" : include "add-coupon.php"; break;

		case "view-coupons" : include "view-coupons.php"; break;

		case "page-about-us" : include "about.php"; break;

		case "page-terms-condition" : include "terms.php"; break;

		case "page-call-support" : include "support.php"; break;

		//case "map" : include "map.php"; break;

		case "driver-map" : include "driver-map.php"; break;

		//case "user-map" : include "user-map.php"; break;

		case "push-messages" : include "push_messages.php"; break;	

		case "view-ride" : include "request.php"; break;
		
		case "language" : include "language.php"; break;

		case "index" : include "index.php"; break;

		case "accounts" : include "accounts.php"; break;

		case "add-role" : include "add_role.php"; break;

		case "view-role" : include "view_role.php"; break;

		case "add-subadmin" : include "add_subadmin.php"; break;

		case "view-subadmin" : include "view_subadmin.php"; break;

        case "add-city" : include "add-city.php"; break;

        case "view-city" : include "view-city.php"; break;

        case "logout" : include "logout.php"; break;

		default : include "404.php";
	}
	}else{

		switch($_REQUEST['pages']) {

			case "dashboard" : include "dashboard.php"; break;
			case "account-setting" : include "account-setting.php"; break;
			case "profile" : include "profile.php"; break;
			case "change-password" : include "change-password.php"; break;
			case "index" : include "index.php"; break;
            case "logout" : include "logout.php"; break;
		}


		$query="select * from role WHERE role_id = '".$_SESSION['ADMIN']['ROLE']."' ";
        $result = $db->query($query);
        $list=$result->rows;
        foreach($list as $lists);
        $data =  json_decode(html_entity_decode($lists['role_permission']), true);

        if($data[user_view] == 1){

		switch($_REQUEST['pages']) {
			case "view-user" : include "users.php"; break;
		}
		}

		if($data[driver_view] == 1){

//		header("Location: home.php?pages=view-driver");	

		switch($_REQUEST['pages']) {
			case "view-driver" : include "drivers.php"; break;
			case "driver-map" : include "driver-map.php"; break;
		}
		}

		if($data[transaction_view] == 1){

		switch($_REQUEST['pages']) {
			case "view-transactions" : include "transactions.php"; break;
		}
		}

        if($data[add_company] == 1){

        switch($_REQUEST['pages']) {
            case "add-company" : include "add-company.php"; break;
        }
        }
        if($data[view_company] == 1){

        switch($_REQUEST['pages']) {
            case "view-company" : include "view-company.php"; break;
        }
        }

		if($data[city_add] == 1){

		switch($_REQUEST['pages']) {
			case "add-city" : include "add-city.php"; break;
		}
		}

		if($data[city_view] == 1){

		switch($_REQUEST['pages']) {
			case "view-city" : include "view-city.php"; break;
		}
		}

		if($data[cartype_add] == 1){

		switch($_REQUEST['pages']) {
			case "add-car-type" : include "add-car.php"; break;
		}
		}

		if($data[cartype_view] == 1){

		switch($_REQUEST['pages']) {
			case "view-car-type" : include "view-car.php"; break;
		}
		}

		if($data[carmodel_add] == 1){

		switch($_REQUEST['pages']) {
			case "add-car-model" : include "add-car-model.php"; break;
		}
		}

		if($data[carmodel_view] == 1){

		switch($_REQUEST['pages']) {
			case "view-car-model" : include "view-car-model.php"; break;
		}
		}

		if($data[coupon_add] == 1){

		switch($_REQUEST['pages']) {
			case "add-coupons" : include "add-coupon.php"; break;
		}
		}

		if($data[coupon_view] == 1){

		switch($_REQUEST['pages']) {
			case "view-coupons" : include "view-coupons.php"; break;
		}
		}

		if($data[ratecard_add] == 1){

		switch($_REQUEST['pages']) {
			case "add-rate-card" : include "add-rate-card.php"; break;
		}
		}

		if($data[ratecard_view] == 1){

		switch($_REQUEST['pages']) {
			case "view-rate-card" : include "view-rate-card.php"; break;
		}
		}

		if($data[view_now] == 1){

		switch($_REQUEST['pages']) {
			case "ride-now" : include "ride-now.php"; break;
		}
		}

		if($data[view_later] == 1){

		switch($_REQUEST['pages']) {
			case "ride-later" : include "ride-later.php"; break;
		}
		}

		if($data[ride_view] == 1){

		switch($_REQUEST['pages']) {
			case "ride-completed" : include "ride-completed.php"; break;
		}
		}

		if($data[about] == 1){

		switch($_REQUEST['pages']) {
			case "page-about-us" : include "about.php"; break;
		}
		}

		if($data[terms] == 1){

		switch($_REQUEST['pages']) {
			case "page-terms-condition" : include "terms.php"; break;
		}
		}

		if($data[call] == 1){

		switch($_REQUEST['pages']) {
			case "page-call-support" : include "support.php"; break;
		}
		}

		if($data[role_add] == 1){

		switch($_REQUEST['pages']) {
			case "add-role" : include "add_role.php"; break;
		}
		}

		if($data[role_view] == 1){

		switch($_REQUEST['pages']) {
			case "view-role" : include "view_role.php"; break;
		}
		}

		if($data[subadmin_add] == 1){

		switch($_REQUEST['pages']) {
			case "add-subadmin" : include "add_subadmin.php"; break;
		}
		}

		if($data[subadmin_view] == 1){

		switch($_REQUEST['pages']) {
			case "view-subadmin" : include "view_subadmin.php"; break;
		}
		}

		if($data[account] == 1){

		switch($_REQUEST['pages']) {
			case "accounts" : include "accounts.php"; break;
		}
		}

	}

?>
