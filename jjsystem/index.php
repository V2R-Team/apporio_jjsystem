<?php 
include_once '../apporioconfig/start_up.php'; 

if(isset($_POST['login']) && $_POST['login'] == "LOGIN") {

$query="select * from admin WHERE admin_username='".$_POST['admin_username']."' and admin_password='".$_POST['admin_password']."'";
	$result = $db->query($query);
	$ex_rows=$result->num_rows;
  $list=$result->rows;
  foreach ($list as $key);
	if($ex_rows==1){
        $_SESSION['ADMIN']['ID'] = $key['admin_id'];
        $_SESSION['ADMIN']['UN'] = $key['admin_fname'];
        $_SESSION['ADMIN']['ROLE'] = $key['admin_role'];
        header("Location: home.php?pages=dashboard");
    }
 }
?>

<script type="text/javascript">
function validatelogin() {
    re = /^[A-Za-z ]+$/;
    te = /^[0-9]+$/;
    se = /^[0-9A-Za-z ]+[A-Za-z ]+$/;
  if( document.getElementById('admin_fname').value == "" ) {
   alert('Please Enter Your UserName');
   document.getElementById('admin_fname').focus();
   return false;
  }
  if(!re.test(document.getElementById('admin_fname').value))
  {
  alert("Please Enter Your Correct UserName");
  document.getElementById(admin_fname).focus();
  return false;
  }

  if(document.getElementById('password').value == "")
  {
  alert("Please Enter Your Password");
  document.getElementById('password').focus();
  return false;
  }
  return true;
}


</script>    
    

<!DOCTYPE html>
<html lang="en">
    

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Taxi Admin">
        <meta name="author" content="">

        <title>JJ System Admin</title>

        <!-- Google-Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic' rel='stylesheet'>


        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/bootstrap-reset.css" rel="stylesheet">

        <!--Animation css-->
        <link href="../css/animate.css" rel="stylesheet">

        <!--Icon-fonts css-->
        <link href="../taxi/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="../taxi/ionicon/css/ionicons.min.css" rel="stylesheet" />

        <!--Morris Chart CSS -->

        <!-- Custom styles for this template -->
        <link href="../css/style.css" rel="stylesheet">
        <link href="../css/helper.css" rel="stylesheet">
        <link href="../css/style-responsive.css" rel="stylesheet" />

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->


    </head>


    <body>

        <div class="wrapper-page animated fadeInDown">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading"> 
                   <h3 class="text-center m-t-10"> <strong>JJ System Login</strong> </h3>
                </div> 

                <form class="form-horizontal m-t-40" method="post" onSubmit="return validatelogin()">
                                            
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username"  name="admin_username" id="admin_fname" />
                        </div>
                    </div>
                    <div class="form-group ">
                        
                        <div class="col-xs-12">
                            
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="admin_password" id="password" />
                        </div>
                    </div>

                   
                    
                    
                    <div class="form-group text-right">
                        <div class="col-xs-12">
                       

  <button class="btn btn-purple w-md" type="submit" name="login" value="LOGIN" id="login">Log In</button> 
                            
                            
                        
                    <div class="form-group text-left">
					<div class="col-sm-7">
                            <span style="cursor:pointer;" data-toggle="modal" data-target="#forgotpassword">Forgot your password?</span> 
                       </div>
                    </div>
					</div>
                  </div>
                    
                </form>

            </div>
        </div>

    

<div class="modal fade" id="forgotpassword" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Forgot your password?</h4>
        </div>
        <div class="panel panel-color panel-primary">

                <form method="post" role="form" class="text-center"> 
                    <div class="alert alert-info alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        Enter your <b>Email</b> and instructions will be sent to you!
                    </div>
                    <div class="form-group m-b-0"> 
                        <div class="input-group"> 
                            <input type="password" class="form-control" placeholder="Enter Email"> 
                            <span class="input-group-btn"> <button type="submit" class="btn btn-primary">Reset</button> </span> 
                        </div> 
                    </div> 
                    
                </form>

                                        
                
            </div>
        
      </div>
      
    </div>
  </div>


        <!-- js placed at the end of the document so the pages load faster -->
        <script src="../js/jquery.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/pace.min.js"></script>
        <script src="../js/wow.min.js"></script>
        <script src="../js/jquery.nicescroll.js" type="text/javascript"></script>
            

        <!--common script for all pages-->
        <script src="../js/jquery.app.js"></script>

    
    </body>

</html>
