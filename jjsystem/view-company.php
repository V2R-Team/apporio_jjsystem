<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');

    $query="select * from company ORDER BY company_id";
	$result = $db->query($query);
	$list=$result->rows;

    if(isset($_GET['status']) && isset($_GET['id'])) 
    {
     $query1="UPDATE company SET company_status='".$_GET['status']."' WHERE company_id='".$_GET['id']."'";
     $db->query($query1);
     $db->redirect("home.php?pages=view-company");
    }

if(isset($_POST['savechanges'])) 
     {
       $query2="UPDATE company SET company_name='".$_POST['company_name']."',company_email='".$_POST['company_email']."',company_phone='".$_POST['company_phone']."',company_address='".$_POST['company_address']."',company_contact_person='".$_POST['company_contact_person']."' where company_id='".$_POST['savechanges']."'";
       $db->query($query2); 
       $db->redirect("home.php?pages=view-company");
     }
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB21baMnSAvRrcZ3IKymum3_vJFN4hm7xU&v=3.exp&libraries=places"></script>

<script>
    function initialize() {

        var input = document.getElementById('company_address');

        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<form method="post" name="frm">
<div class="wraper container-fluid">
  <div class="page-title">
    <h3 class="title">View Company Details</h3>
  
      </div>
  
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
              <table id="datatable" class="table table-striped table-bordered table-responsive">
                <thead>
                  <tr>
                      <th>S.No</th>
                      <th>Company Name</th>
                      <th>Company Email</th>
                      <th>Company Phone</th>
                      <th>Company Address</th>
                      <th>Company Contact Person</th>
                      <th>Status</th>
                      <th>Edit</th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                  $i = 1;
                  foreach($list as $company){?>
                  <tr>
                      <td><?php echo $i;?></td>
                      <td><?= $company['company_name'];?></td>
                      <td><?= $company['company_email'];?></td>
                      <td><?= $company['company_phone'];?></td>
                      <td><?= $company['company_address'];?></td>
                      <td><?= $company['company_contact_person'];?></td>
                    <?php
                                if($company['company_status']==1) {
                                ?>
                                <td class="text-center">
                                    <a href="home.php?pages=view-company&status=2&id=<?php echo $company['company_id']?>" class="" title="Active">
                                    <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active
                                    </button></a>
                                </td>
                                <?php
                                } else {
                                ?>
                                <td class="text-center">
                                <a href="home.php?pages=view-company&status=1&id=<?php echo $company['company_id']?>" class="" title="Deactive">
                                    <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive
                                    </button></a>
                                </td>
                            <?php } ?>
 <td><button type="button" class="btn btn-info glyphicon glyphicon-pencil" data-toggle="modal" data-target="#<?php echo $company['company_id'];?>"  ></button></td>
                  </tr>
                  <?php $i++;
                         }
                    ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End row --> 
  
</div>
</form>

<?php foreach($list as $company){?>
<div class="modal fade" id="<?php echo $company['company_id'];?>" role="dialog">
  <div class="modal-dialog"> 
    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title fdetailsheading">Edit Company Details</h4>
      </div>
      <form  method="post" >
        <div class="modal-body">

          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="field-3" class="control-label">Company Name</label>
                <input type="text" class="form-control"  placeholder="Company Name" name="company_name" value="<?php echo $company['company_name'];?>" id="company_name" required>
              </div>
            </div>
          </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="field-3" class="control-label">Company Email</label>
                        <input type="text" class="form-control"  placeholder="Company Email" name="company_email" value="<?php echo $company['company_email'];?>" id="company_email" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="field-3" class="control-label">Company Phone</label>
                        <input type="text" class="form-control"  placeholder="Company Phone" name="company_phone" value="<?php echo $company['company_phone'];?>" id="company_phone" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="field-3" class="control-label">Company Address</label>
                        <input type="text" class="form-control"  placeholder="Company Address" name="company_address" value="<?php echo $company['company_address'];?>" id="company_address" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="field-3" class="control-label">Company Contact Person</label>
                        <input type="text" class="form-control"  placeholder="Company Contact Person" name="company_contact_person" value="<?php echo $company['company_contact_person'];?>" id="company_contact_person" required>
                    </div>
                </div>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
          <button type="submit" name="savechanges" value="<?php echo $company['company_id'];?>" class="btn btn-info">Save Changes</button>
        </div>
      </form>
    </div>
    
  </div>
</div>
<?php }?>
<!-- Page Content Ends --> 
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>